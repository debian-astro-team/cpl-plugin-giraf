# GIRAFFE_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([GIRAFFE_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# GIRAFFE_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([GIRAFFE_SET_VERSION_INFO],
[
    giraffe_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    giraffe_major_version=`echo "$giraffe_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    giraffe_minor_version=`echo "$giraffe_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    giraffe_micro_version=`echo "$giraffe_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$giraffe_major_version"; then
        giraffe_major_version=0
    fi

    if test -z "$giraffe_minor_version"; then
        giraffe_minor_version=0
    fi

    if test -z "$giraffe_micro_version"; then
        giraffe_micro_version=0
    fi

    GIRAFFE_VERSION="$giraffe_version"
    GIRAFFE_MAJOR_VERSION=$giraffe_major_version
    GIRAFFE_MINOR_VERSION=$giraffe_minor_version
    GIRAFFE_MICRO_VERSION=$giraffe_micro_version

    if test -z "$4"; then
        GIRAFFE_INTERFACE_AGE=0
    else
        GIRAFFE_INTERFACE_AGE="$4"
    fi

    GIRAFFE_BINARY_AGE=`expr 100 '*' $GIRAFFE_MINOR_VERSION + $GIRAFFE_MICRO_VERSION`
    GIRAFFE_BINARY_VERSION=`expr 10000 '*' $GIRAFFE_MAJOR_VERSION + \
                          $GIRAFFE_BINARY_AGE`

    AC_SUBST(GIRAFFE_VERSION)
    AC_SUBST(GIRAFFE_MAJOR_VERSION)
    AC_SUBST(GIRAFFE_MINOR_VERSION)
    AC_SUBST(GIRAFFE_MICRO_VERSION)
    AC_SUBST(GIRAFFE_INTERFACE_AGE)
    AC_SUBST(GIRAFFE_BINARY_VERSION)
    AC_SUBST(GIRAFFE_BINARY_AGE)

    AC_DEFINE_UNQUOTED(GIRAFFE_MAJOR_VERSION, $GIRAFFE_MAJOR_VERSION,
                       [GIRAFFE major version number])
    AC_DEFINE_UNQUOTED(GIRAFFE_MINOR_VERSION, $GIRAFFE_MINOR_VERSION,
                       [GIRAFFE minor version number])
    AC_DEFINE_UNQUOTED(GIRAFFE_MICRO_VERSION, $GIRAFFE_MICRO_VERSION,
                       [GIRAFFE micro version number])
    AC_DEFINE_UNQUOTED(GIRAFFE_INTERFACE_AGE, $GIRAFFE_INTERFACE_AGE,
                       [GIRAFFE interface age])
    AC_DEFINE_UNQUOTED(GIRAFFE_BINARY_VERSION, $GIRAFFE_BINARY_VERSION,
                       [GIRAFFE binary version number])
    AC_DEFINE_UNQUOTED(GIRAFFE_BINARY_AGE, $GIRAFFE_BINARY_AGE,
                       [GIRAFFE binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# GIRAFFE_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([GIRAFFE_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi
    AC_SUBST(plugindir)
    
    if test -z "$configdir"; then
       configdir='${datadir}/${PACKAGE}/config'
    fi
    AC_SUBST(configdir)

    if test -z "$pipelinelibdir"; then
    	pipelinelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi
    AC_SUBST(pipelinelibdir)

    if test -z "$pipelinedocdir"; then
        pipelinedocdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}'
    fi
    AC_SUBST(pipelinedocdir)

	if test -z "$reflexdatadir"; then
		reflexdatadir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
	fi
    AC_SUBST(reflexdatadir)
	
	if test -z "$workflowdir"; then
		workflowdir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
	fi
    AC_SUBST(workflowdir)

    if test -z "$apidocdir"; then
        apidocdir='${pipelinedocdir}/html'
    fi
    AC_SUBST(apidocdir)
    

    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(GIRAFFE_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(GIRAFFE_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# GIRAFFE_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([GIRAFFE_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    GIRAFFE_INCLUDES='-I$(top_srcdir)/giraffe -I$(top_srcdir)/irplib'
    GIRAFFE_LDFLAGS='-L$(top_srcdir)/giraffe'

    # Library aliases

    LIBGIRAFFE='$(top_builddir)/giraffe/libgiraffe.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'

    # Using irplib_fits_update_checksums function:
    IRPLIB_CPPFLAGS='-DIRPLIB_USE_FITS_UPDATE_CHECKSUM'

    # Substitute the defined symbols

    AC_SUBST(GIRAFFE_INCLUDES)
    AC_SUBST(GIRAFFE_LDFLAGS)

    AC_SUBST(LIBGIRAFFE)
    AC_SUBST(LIBIRPLIB)
    AC_SUBST(IRPLIB_CPPFLAGS)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(GIRAFFE_INCLUDES) $(CPL_INCLUDES) $(CFITSIO_INCLUDES) $(CX_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(GIRAFFE_LDFLAGS) $(CPL_LDFLAGS) $(CX_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])


# GIRAFFE_CHECK_CPL(version)
#------------------------------
# Checks for the CPL library
AC_DEFUN([GIRAFFE_CHECK_CPL],
[
    giraffe_cpl_check_version="$1"


    AC_REQUIRE([CPL_CHECK_CFITSIO])
    AC_REQUIRE([CPL_CHECK_CEXT])
    CPL_CHECK_LIBS([$giraffe_cpl_check_version])
    
])


# GIRAFFE_SET_DID(SYMBOL, NAME)
#------------------------------
# Adds a DID identifier string to config.h
AC_DEFUN([GIRAFFE_SET_DID],
[

    if test -n "$1" || test -n "$2"; then
        AC_DEFINE_UNQUOTED($1, "$2",
                  [ESO DID identifier the library complies to])
        eval "$1"="$2"
        AC_SUBST($1)
    fi

])
