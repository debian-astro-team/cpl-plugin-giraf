## Process this file with automake to produce Makefile.in

## This file is part of the GIRAFFE Pipeline Library
## Copyright (C) 2002-2019 European Southern Observatory

## This library is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~


if MAINTAINER_MODE

MAINTAINERCLEANFILES = $(srcdir)/Makefile.in

endif


LIBGIRAFFE = $(top_builddir)/giraffe/libgiraffe.la

AM_CPPFLAGS = -I$(top_srcdir)/giraffe -I$(top_srcdir)/recipes \
              -I$(top_srcdir)/irplib $(CPL_INCLUDES) $(IRPLIB_CPPFLAGS)

LDADD = $(LIBGIRAFFE) $(LIBCPLCORE) $(LIBCPLUI)

check_PROGRAMS = giraffe_sdp-test

giraffe_sdp_test_SOURCES = giraffe_sdp-test.c
giraffe_sdp_test_LDFLAGS = $(CPL_LDFLAGS)
giraffe_sdp_test_LDADD = $(LDADD) $(LIBIRPLIB) $(LIBCFITSIO) $(LIBCEXT)

# Be sure to reexport important environment variables.
TESTS_ENVIRONMENT = MAKE="$(MAKE)" CC="$(CC)" CFLAGS="$(CFLAGS)" \
        CPPFLAGS="$(CPPFLAGS)" LD="$(LD)" LDFLAGS="$(LDFLAGS)" \
        LIBS="$(LIBS)" LN_S="$(LN_S)" NM="$(NM)" RANLIB="$(RANLIB)" \
        OBJEXT="$(OBJEXT)" EXEEXT="$(EXEEXT)"

TESTS = $(check_PROGRAMS)

# We need to remove any files that the above tests created.
clean-local:
	$(RM) *.fits *.log
