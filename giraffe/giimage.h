/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIIMAGE_H
#define GIIMAGE_H

#include <cxtypes.h>
#include <cxstring.h>

#include <cpl_macros.h>
#include <cpl_type.h>
#include <cpl_image.h>
#include <cpl_propertylist.h>
#include <cpl_frameset.h>

#include <giutils.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef struct GiImage GiImage;


GiImage *giraffe_image_new(cpl_type type);
GiImage *giraffe_image_create(cpl_type type, cxint nx, cxint ny);
GiImage *giraffe_image_duplicate(const GiImage *image);
void giraffe_image_delete(GiImage *self);

cpl_image *giraffe_image_get(const GiImage *self);
cxint giraffe_image_set(GiImage *self, cpl_image *image);

cpl_propertylist *giraffe_image_get_properties(const GiImage *self);
cxint giraffe_image_set_properties(GiImage *self,
                                   cpl_propertylist *properties);

cxint giraffe_image_copy_matrix(GiImage *self, cpl_matrix *matrix);

cxint giraffe_image_load_pixels(GiImage *self, const cxchar *filename,
                                cxint position, cxint plane);
cxint giraffe_image_load_properties(GiImage *self, const cxchar *filename,
                                    cxint position);

cxint giraffe_image_load(GiImage *self, const cxchar *filename,
                         cxint position);
cxint giraffe_image_save(GiImage *self, const cxchar *filename);


cxint giraffe_image_paste(GiImage *self, const GiImage *image, cxint x,
                          cxint y, cxbool clip);

void giraffe_image_print(GiImage *self);

cxint giraffe_image_add_info(GiImage *, const GiRecipeInfo *,
                             const cpl_frameset *);

#ifdef __cplusplus
}
#endif

#endif /* GIIMAGE_H */
