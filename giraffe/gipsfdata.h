/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIPSFDATA_H
#define GIPSFDATA_H

#include <cxtypes.h>

#include <cpl_image.h>


#ifdef __cplusplus
extern "C" {
#endif

    typedef struct GiPsfData GiPsfData;


    GiPsfData* giraffe_psfdata_new(void);
    GiPsfData* giraffe_psfdata_create(cxint nfibers, cxint nbins,
                                      cxint nx, cxint ny);
    void giraffe_psfdata_delete(GiPsfData* self);
    void giraffe_psfdata_clear(GiPsfData* self);

    void giraffe_psfdata_resize(GiPsfData* self, cxint nfibers, cxint nbins,
                                cxint nx, cxint ny);

    cxsize giraffe_psfdata_bins(const GiPsfData* self);
    cxsize giraffe_psfdata_fibers(const GiPsfData* self);
    cxsize giraffe_psfdata_xsize(const GiPsfData* self);
    cxsize giraffe_psfdata_ysize(const GiPsfData* self);
    cxsize giraffe_psfdata_parameters(const GiPsfData* self);

    cxbool giraffe_psfdata_contains(const GiPsfData* self,
                                    const cxchar* name);

    const cxchar* giraffe_psfdata_get_name(const GiPsfData* self,
                                           cxsize position);

    cxint giraffe_psfdata_set_model(GiPsfData* self, const cxchar* name);
    const cxchar* giraffe_psfdata_get_model(const GiPsfData* self);

    cxint giraffe_psfdata_set_bin(GiPsfData* self, cxint fiber, cxint bin,
                                  cxdouble position);
    cxdouble giraffe_psfdata_get_bin(const GiPsfData* self,
                                     cxint fiber, cxint bin);
    const cpl_image* giraffe_psfdata_get_bins(const GiPsfData* self);

    cxint giraffe_psfdata_set(GiPsfData* self, const cxchar* name,
                              cxint fiber, cxint bin, cxdouble value);
    cxdouble giraffe_psfdata_get(const GiPsfData* self, const cxchar* name,
                                 cxint fiber, cxint bin);

    cxint giraffe_psfdata_set_data(GiPsfData* self, const cxchar* name,
                                   const cpl_image* values);
    const cpl_image* giraffe_psfdata_get_data(const GiPsfData* self,
                                              const cxchar* name);

    cxint giraffe_psfdata_load(GiPsfData* self, const cxchar* filename);
    cxint giraffe_psfdata_save(const GiPsfData* self,
                               cpl_propertylist* properties,
                               const cxchar* filename, cxcptr data);

#ifdef __cplusplus
}
#endif

#endif /* GIPSFDATA_H */
