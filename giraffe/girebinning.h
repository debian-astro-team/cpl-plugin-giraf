/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIREBINNING_H
#define GIREBINNING_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_matrix.h>
#include <cpl_parameterlist.h>

#include <giimage.h>
#include <girange.h>
#include <gilocalization.h>
#include <giextraction.h>


#ifdef __cplusplus
extern "C" {
#endif


#define O_NX          0     /* Optical parameters */
#define O_PXSIZ       1
#define O_FCOLL       2
#define O_CFACT       3
#define O_SOFFX       4
#define O_SOFFY       5
#define O_SPHI        6
#define O_OPT_NPRMS   7

#define G_THETA       0     /* Grating parameters */
#define G_ORDER       1
#define G_WLMIN       2
#define G_WLEN0       3
#define G_WLMAX       4
#define G_RESOL       5
#define G_SPACE       6
#define G_GRAT_NPRMS  7

/* optical model parameters indices */

#define OG_NX           0
#define OG_PXSIZ        1
#define OG_FCOLL        2
#define OG_CFACT        3
#define OG_THETA        4
#define OG_ORDER        5
#define OG_SPACE        6
#define OG_SOFFX        7
#define OG_SOFFY        8
#define OG_SPHI         9
#define OG_OPTM_NPRMS  10

#define GIREBIN_SIZE_Y_DEFAULT 5600

enum GiRebinMethod {
    GIREBIN_METHOD_UNDEFINED,
    GIREBIN_METHOD_LINEAR,
    GIREBIN_METHOD_SPLINE
};

typedef enum GiRebinMethod GiRebinMethod;

enum GiRebinScale {
    GIREBIN_SCALE_UNDEFINED,
    GIREBIN_SCALE_LOG,
    GIREBIN_SCALE_LINEAR
};

typedef enum GiRebinScale GiRebinScale;

enum GiRebinRange {
    GIREBIN_RANGE_UNDEFINED,
    GIREBIN_RANGE_SETUP,
    GIREBIN_RANGE_COMMON
};

typedef enum GiRebinRange GiRebinRange;

struct GiRebinConfig {
    GiRebinMethod  rmethod;
    cxbool         xresiduals;
    cxdouble       lstep;
    GiRebinScale   scmethod;
    cxint          size;
    GiRebinRange   range;
};

typedef struct GiRebinConfig GiRebinConfig;

struct GiRebinning {
    GiImage *spectra;
    GiImage *errors;
};

typedef struct GiRebinning GiRebinning;

struct GiRebinParams {
    cxdouble  min;       /**< lambda min                      */
    cxdouble  step;      /**< lambda step                     */
    cxint     log;       /**< 1: loglambda, 0: lambda         */
    cxint     xres;      /**< 1: take care of  X residuals    */
    cxint     size;      /**< size of rebinned spectra        */
};

typedef struct GiRebinParams GiRebinParams;


GiRange *giraffe_rebin_get_wavelength_range(GiImage *spectra,
                                            GiTable *wlsolution,
                                            GiTable *grating,
                                            GiTable *slitgeometry,
                                            cxbool common);

cxint giraffe_rebin_spectra(GiRebinning *rebinning,
                            const GiExtraction *extraction,
                            const GiTable *fibers,
                            const GiLocalization *localization,
                            const GiTable *grating,
                            const GiTable *slitgeo,
                            const GiTable *solution,
                            const GiRebinConfig *config);


/*
 * Convenience functions
 */

GiRebinning *giraffe_rebinning_new(void);
GiRebinning *giraffe_rebinning_create(GiImage *spectra, GiImage *errors);
void giraffe_rebinning_delete(GiRebinning *rebinning);
void giraffe_rebinning_destroy(GiRebinning *rebinning);

GiRebinConfig *giraffe_rebin_config_create(cpl_parameterlist *list);
void giraffe_rebin_config_destroy(GiRebinConfig *config);

void giraffe_rebin_config_add(cpl_parameterlist *list);


#ifdef __cplusplus
}
#endif

#endif /* GIREBINNING_H */
