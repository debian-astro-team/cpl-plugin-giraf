/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxtypes.h>
#include <cxmemory.h>

#include "giextraction.h"


/*
 * @defgroup giextraction Spectrum Extraction Data Storage
 *
 * TBD
 */

/**@{*/

/**
 * @brief
 *   Creates an empty extraction results container.
 *
 * @return A newly allocated extraction results container.
 *
 * The function allocales memory for a extraction results container
 * The container is initialized to be empty.
 */

GiExtraction*
giraffe_extraction_new(void)
{

    GiExtraction* self = cx_malloc(sizeof *self);

    self->spectra = NULL;
    self->error = NULL;
    self->npixels = NULL;
    self->centroid = NULL;
    self->model = NULL;

    return self;

}


/**
 * @brief
 *   Creates a filled extraction results container.
 *
 * @param spectra   Extracted spectra.
 * @param error     Errors of the extracted spectra.
 * @param npixels   Pixels used for spectrum extraction.
 * @param centroid  Extraction centroid.
 * @param model     Model spectra.
 *
 * @return A newly allocated extraction results container.
 *
 * The function allocales memory for a extraction results container
 * The container is filled with the given extraction parameter images.
 * Only the references to the different extraction components are stored.
 */

GiExtraction*
giraffe_extraction_create(GiImage* spectra, GiImage* error, GiImage* npixels,
                          GiImage* centroid, GiImage* model)
{

    GiExtraction* self = giraffe_extraction_new();


    if (spectra != NULL) {
        self->spectra = spectra;
    }

    if (error != NULL) {
        self->error = error;
    }

    if (npixels != NULL) {
        self->npixels = npixels;
    }

    if (centroid != NULL) {
        self->centroid = centroid;
    }

    if (model != NULL) {
        self->model = model;
    }

    return self;

}


/**
 * @brief
 *   Destroys a extraction results container.
 *
 * @param extraction  The extraction results container to destroy.
 *
 * @return Nothing.
 *
 * The function deallocates the memory used for the extraction results
 * container @em extraction. Only the container itself is detroyed by
 * calling this function. Since the container stores only references to the
 * extraction components its contents is left untouched and it is the
 * responsibility of the caller to ensure that other references for the
 * stored extraction components exist.
 */

void
giraffe_extraction_delete(GiExtraction* extraction)
{

    if (extraction != NULL) {
        cx_free(extraction);
    }

    return;

}


/**
 * @brief
 *   Destroys a extraction results container and its contents.
 *
 * @param extraction  The extraction results container to destroy.
 *
 * @return Nothing.
 *
 * The function deallocates the memory used for the extraction results
 * container @em extraction and each extraction component it possibly
 * contains.
 */

void
giraffe_extraction_destroy(GiExtraction* extraction)
{

    if (extraction != NULL) {

        if (extraction->spectra != NULL) {
            giraffe_image_delete(extraction->spectra);
            extraction->spectra = NULL;
        }

        if (extraction->error != NULL) {
            giraffe_image_delete(extraction->error);
            extraction->error = NULL;
        }

        if (extraction->npixels != NULL) {
            giraffe_image_delete(extraction->npixels);
            extraction->npixels = NULL;
        }

        if (extraction->centroid != NULL) {
            giraffe_image_delete(extraction->centroid);
            extraction->centroid = NULL;
        }

        if (extraction->model != NULL) {
            giraffe_image_delete(extraction->model);
            extraction->model = NULL;
        }

        cx_free(extraction);
    }

    return;

}
/**@}*/
