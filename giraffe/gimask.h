/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIMASK_H
#define GIMASK_H

#ifdef __cplusplus
extern "C" {
#endif

    enum GiMaskType {
        GIMASK_FITTED_DATA,
        GIMASK_FIT_COEFFS
    };

    typedef enum GiMaskType GiMaskType;


    struct GiMaskParameters {
        cxint ywidth;
        cxint method;
        cxdouble threshold;
        cxint ydeg;
        cxint wdeg;
        cxint start;
        cxint retry;
        cxint xbin;
        cxdouble ewid;
        cxdouble wavg;
        cxdouble sigma;
        cxdouble mfrac;
        cxint niter;

        struct {
            cxint width;
            cxint height;
            cxsize count;
        } ckdata;
    };

    typedef struct GiMaskParameters GiMaskParameters;


    struct GiMaskPosition {
        GiMaskType type;
        cpl_matrix *my;
        cpl_matrix *mw;
    };

    typedef struct GiMaskPosition GiMaskPosition;


#ifdef __cplusplus
}
#endif

#endif /* GIMASK_H */
