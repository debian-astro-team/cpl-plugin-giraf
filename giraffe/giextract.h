/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIEXTRACT_H
#define GIEXTRACT_H

#include <cpl_macros.h>
#include <cpl_parameterlist.h>

#include <giimage.h>
#include <gitable.h>
#include <gilocalization.h>
#include <giextraction.h>


#ifdef __cplusplus
extern "C" {
#endif


enum GiExtractMethod {
    GIEXTRACT_SUM,
    GIEXTRACT_HORNE,
    GIEXTRACT_OPTIMAL
};

typedef enum GiExtractMethod GiExtractMethod;


struct GiExtractConfig {
    GiExtractMethod emethod;

    cxdouble ron;

    struct {
        cxchar* model;
        cxdouble sigma;
        cxint iterations;
    } psf;

    struct {
        cxint ewidth;
        cxint mingood;
    } horne;

    struct {
        cxint bkgorder;
        cxdouble wfactor;
        cxdouble fraction;
    } optimal;

};

typedef struct GiExtractConfig GiExtractConfig;


/*
 * Spectrum extraction
 */

cxint giraffe_extract_spectra(GiExtraction* result, GiImage* image,
                              GiTable* fibers, GiLocalization* sloc,
                              GiImage* bpixel, GiImage* slight,
                              GiExtractConfig* config);

/*
 * Convenience functions
 */

GiExtractConfig* giraffe_extract_config_create(cpl_parameterlist* list);
void giraffe_extract_config_destroy(GiExtractConfig* config);

void giraffe_extract_config_add(cpl_parameterlist *list);

#ifdef __cplusplus
}
#endif

#endif /* GIEXTRACT_H */
