/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GILINEDATA_H
#define GILINEDATA_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_image.h>
#include <cpl_table.h>


#ifdef __cplusplus
extern "C" {
#endif

    typedef struct GiLineData GiLineData;


    GiLineData* giraffe_linedata_new(void);
    GiLineData* giraffe_linedata_create(const cpl_table* lines,
                                        const cpl_table* fibers,
                                        const cxchar* model);
    void giraffe_linedata_delete(GiLineData* self);

    cxint giraffe_linedata_reset(GiLineData* self, const cpl_table* lines,
                                 const cpl_table* fibers, const cxchar* model);

    const cxchar* giraffe_linedata_model(const GiLineData* self);
    cxsize giraffe_linedata_lines(const GiLineData* self);
    cxsize giraffe_linedata_fibers(const GiLineData* self);
    cxbool giraffe_linedata_contains(GiLineData* self, const cxchar* name);

    cxsize giraffe_linedata_accepted(const GiLineData* self);
    cxsize giraffe_linedata_rejected(const GiLineData* self);

    cpl_image* giraffe_linedata_status(const GiLineData* self);

    cxint giraffe_linedata_set_status(GiLineData* self, cxint fiber,
                                      cxint line, cxint status);
    cxint giraffe_linedata_get_status(const GiLineData* self, cxint fiber,
                                      cxint line);

    cxint giraffe_linedata_set_wavelength(GiLineData* self, cxint line,
                                          cxdouble lambda);
    cxdouble giraffe_linedata_get_wavelength(const GiLineData* self,
                                             cxint line);

    cxint giraffe_linedata_set(GiLineData* self, const cxchar* name,
                               cxint fiber, cxint line, cxdouble value);
    cxdouble giraffe_linedata_get(const GiLineData* self, const cxchar* name,
                                  cxint fiber, cxint line);

    cxint giraffe_linedata_set_data(GiLineData* self, const cxchar* name,
                                    const cpl_image* values);
    const cpl_image* giraffe_linedata_get_data(const GiLineData* self,
                                               const cxchar* name);

    cxint giraffe_linedata_load(GiLineData* self, const cxchar* filename);
    cxint giraffe_linedata_save(GiLineData* self,
                                const cpl_propertylist* properties,
                                const cxchar* filename);

    cxint giraffe_linedata_writer(const GiLineData* self,
                                  cpl_propertylist* properties,
                                  const cxchar* filename, cxcptr data);

#ifdef __cplusplus
}
#endif

#endif /* GILINEDATA_H */
