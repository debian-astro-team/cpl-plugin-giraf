/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxtypes.h>
#include <cxmemory.h>

#include "gilocalization.h"


/*
 * @defgroup gilocalization Fiber Tracing Data Storage
 *
 * TBD
 */

/**@{*/

/**
 * @brief
 *   Creates an empty localization results container.
 *
 * @return A newly allocated localization results container.
 *
 * The function allocales memory for a localization results container
 * The container is initialized to be empty.
 */

GiLocalization*
giraffe_localization_new(void)
{

    GiLocalization* self = cx_malloc(sizeof *self);

    self->locy = NULL;
    self->locw = NULL;
    self->locc = NULL;
    self->psf = NULL;

    return self;

}


/**
 * @brief
 *   Creates a filled localization results container.
 *
 * @param locy   Spectrum positions
 * @param locw   Spectrum widths
 * @param locc   Coefficients table fitted positions and widths
 * @param psf    PSF parameters
 *
 * @return A newly allocated localization results container.
 *
 * The function allocales memory for a localization results container
 * The container is filled with the given localization parameter images
 * and tables. Only the references to the different localization components
 * are stored.
 */

GiLocalization*
giraffe_localization_create(GiImage* locy, GiImage* locw, GiTable* locc,
                            GiPsfData* psf)
{

    GiLocalization* self = giraffe_localization_new();


    if (locy != NULL) {
        self->locy = locy;
    }

    if (locw != NULL) {
        self->locw = locw;
    }

    if (locc != NULL) {
        self->locc = locc;
    }

    if (psf != NULL) {
        self->psf = psf;
    }

    return self;

}


/**
 * @brief
 *   Destroys a localization results container.
 *
 * @param localization  The localization results container to destroy.
 *
 * @return Nothing.
 *
 * The function deallocates the memory used for the localization results
 * container @em localization. Only the container itself is detroyed by
 * calling this function. Since the container stores only references to the
 * localization components its contents is left untouched and it is the
 * responsibility of the caller to ensure that other references for the
 * stored localization components exist.
 */

void
giraffe_localization_delete(GiLocalization* localization)
{

    if (localization != NULL) {
        cx_free(localization);
    }

    return;

}


/**
 * @brief
 *   Destroys a localization results container and its contents.
 *
 * @param localization  The localization results container to destroy.
 *
 * @return Nothing.
 *
 * The function deallocates the memory used for the localization results
 * container @em localization and each localization component it possibly
 * contains.
 */

void
giraffe_localization_destroy(GiLocalization* localization)
{

    if (localization != NULL) {

        if (localization->locy != NULL) {
            giraffe_image_delete(localization->locy);
            localization->locy = NULL;
        }

        if (localization->locw != NULL) {
            giraffe_image_delete(localization->locw);
            localization->locw = NULL;
        }

        if (localization->locc != NULL) {
            giraffe_table_delete(localization->locc);
            localization->locc = NULL;
        }

        if (localization->psf != NULL) {
            giraffe_psfdata_delete(localization->psf);
            localization->psf = NULL;
        }

        cx_free(localization);
    }

    return;

}
/**@}*/
