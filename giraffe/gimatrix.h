/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIMATRIX_H
#define GIMATRIX_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_matrix.h>
#include <cpl_image.h>


#ifdef __cplusplus
extern "C" {
#endif


    /*
     * Matrix utilities
     */

    cxdouble giraffe_matrix_sigma_mean(const cpl_matrix*, cxdouble);
    cxdouble giraffe_matrix_sigma_fit(const cpl_matrix*,
                                      const cpl_matrix*);
    cxint giraffe_matrix_sort(cpl_matrix*);

    cpl_matrix *giraffe_matrix_leastsq(const cpl_matrix*,
                                       const cpl_matrix*);

    cpl_matrix*
    giraffe_matrix_solve_cholesky(const cpl_matrix* A, const cpl_matrix* b,
                                  const cpl_matrix* Cb, cpl_matrix* Cx);

    cxint giraffe_matrix_clear(cpl_matrix* matrix);

    void giraffe_matrix_dump(const cpl_matrix* matrix, cxint max_rows);


    /*
     * Matrix conversions
     */

    cpl_image *giraffe_matrix_create_image(const cpl_matrix* matrix);


#ifdef __cplusplus
}
#endif

#endif /* GIMATRIX_H */
