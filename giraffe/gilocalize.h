/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GILOCALIZE_H
#define GILOCALIZE_H

#include <cpl_parameterlist.h>

#include <giimage.h>
#include <gitable.h>
#include <gilocalization.h>


#ifdef __cplusplus
extern "C" {
#endif


struct GiLocalizeConfig {
    cxbool       full;
    cxint        start;
    cxint        retries;
    cxint        binsize;
    cxdouble     ewidth;
    cxint        ywidth;
    cxbool       centroid;
    cxbool       normalize;
    cxint        threshold;
    cxdouble     noise;
    cxdouble     ron;
    cxint        yorder;
    cxint        worder;
    cxdouble     sigma;
    cxint        iterations;
    cxdouble     fraction;
};

typedef struct GiLocalizeConfig GiLocalizeConfig;


/*
 * Spectrum localization
 */

cxint giraffe_localize_spectra(GiLocalization* result, GiImage* image,
                               GiTable* fibers, GiLocalization* master,
                               GiImage* badpixels, GiLocalizeConfig* config);


/*
 * Convenience functions
 */

GiLocalizeConfig* giraffe_localize_config_create(cpl_parameterlist* list);
void giraffe_localize_config_destroy(GiLocalizeConfig* config);

void giraffe_localize_config_add(cpl_parameterlist* list);


#ifdef __cplusplus
}
#endif

#endif /* GILOCALIZE_H */
