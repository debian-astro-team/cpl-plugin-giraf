/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GICHEBYSHEV_H
#define GICHEBYSHEV_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_matrix.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef struct GiChebyshev2D GiChebyshev2D;


GiChebyshev2D *giraffe_chebyshev2d_new(cxint xorder, cxint yorder);
GiChebyshev2D *giraffe_chebyshev2d_clone(const GiChebyshev2D *other);
void giraffe_chebyshev2d_delete(GiChebyshev2D *self);

void giraffe_chebyshev2d_get_order(const GiChebyshev2D *self, cxint *xorder,
                                   cxint *yorder);
void giraffe_chebyshev2d_get_range(const GiChebyshev2D *self, cxdouble *ax,
                                   cxdouble *bx, cxdouble *ay, cxdouble *by);

const cpl_matrix *giraffe_chebyshev2d_coeffs(const GiChebyshev2D *self);

cxint giraffe_chebyshev2d_set(GiChebyshev2D *self, cxdouble ax, cxdouble bx,
                              cxdouble ay, cxdouble by, cpl_matrix *coeffs);
cxint giraffe_chebyshev2d_set_coeff(GiChebyshev2D *self, cxint i, cxint j,
                                    cxdouble value);

cxdouble giraffe_chebyshev2d_eval(const GiChebyshev2D *self, cxdouble x,
                                  cxdouble y);


#ifdef __cplusplus
}
#endif

#endif /* GICHEBYSHEV_H */
