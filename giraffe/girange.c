/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxmessages.h>
#include <cxmemory.h>

#include <cpl_error.h>

#include "girange.h"


/**
 * @defgroup girange A Range Data Type
 *
 * TBD
 */

/**@{*/

struct GiRange {
    cxdouble min;
    cxdouble max;
};


/**
 * @brief
 *   Creates a new range.
 *
 * @return A newly allocated range if no errors occurred, or @c NULL
 *   otherwise.
 *
 * The function creates a new range object. The minimum and maximum values
 * of the created range are initialized to 0.0.
 */

GiRange *
giraffe_range_new(void)
{

    GiRange *self = cx_calloc(1, sizeof *self);
    return self;

}


/**
 * @brief
 *   Creates a new range from the given minimum and maximum values.
 *
 * @param min  The minimum value of the range.
 * @param max  The maximum value of the range.
 *
 * @return A newly allocated range if no errors occurred, or @c NULL
 *   otherwise.
 *
 * The function creates a new range object. The minimum and maximum values
 * of the created range are initialized with the given minimum value @em min
 * and the maximum value @em max.
 */

GiRange *
giraffe_range_create(cxdouble min, cxdouble max)
{

    const cxchar *fctid = "giraffe_range_create";

    GiRange *self = NULL;


    if (min > max) {
        cpl_error_set(fctid, CPL_ERROR_ILLEGAL_INPUT);
        return NULL;
    }

    self = cx_calloc(1, sizeof *self);

    self->min = min;
    self->max = max;

    return self;

}


/**
 * @brief
 *   Destroys a range object.
 *
 * @param self  The range to destroy.
 *
 * @return Nothing.
 *
 * The function deallocates the memory used by the range object @em self.
 */

void
giraffe_range_delete(GiRange *self)
{

    if (self) {
        cx_free(self);
    }

    return;

}


/**
 * @brief
 *   Set the minimum of a range.
 *
 * @param self  The range to be updated.
 * @param min   The value to be set as minimum.
 *
 * @return Nothing.
 *
 * The function updates the minimum value of the given range @em self with
 * @em min.
 */

void
giraffe_range_set_min(GiRange *self, cxdouble min)
{

    cx_assert(self != NULL);

    self->min = min;
    return;

}


/**
 * @brief
 *   Get the minimum of a range.
 *
 * @param self  The range to be queried.
 *
 * @return The current minimum of the range.
 *
 * The function queries the minimum value of the given range @em self.
 */

cxdouble
giraffe_range_get_min(const GiRange *const self)
{

    cx_assert(self != NULL);
    return self->min;

}


/**
 * @brief
 *   Set the maximum of a range.
 *
 * @param self  The range to be updated.
 * @param max   The value to be set as maximum.
 *
 * @return Nothing.
 *
 * The function updates the maximum value of the given range @em self with
 * @em max.
 */

void
giraffe_range_set_max(GiRange *self, cxdouble max)
{

    cx_assert(self != NULL);

    self->max = max;
    return;

}


/**
 * @brief
 *   Get the maximum of a range.
 *
 * @param self  The range to be queried.
 *
 * @return The current maximum of the range.
 *
 * The function queries the maximum value of the given range @em self.
 */

cxdouble
giraffe_range_get_max(const GiRange *const self)
{

    cx_assert(self != NULL);
    return self->max;

}
/**@}*/
