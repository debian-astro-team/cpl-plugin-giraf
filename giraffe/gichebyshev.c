/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxmemory.h>
#include <cxmessages.h>

#include <cpl_matrix.h>

#include "gichebyshev.h"


/**
 * defgroup gichebyshev Chebyshev Polynomials
 *
 * TBD
 */

/**@{*/

struct GiChebyshev2D {

    cxint xorder;
    cxint yorder;

    cxdouble ax;
    cxdouble bx;
    cxdouble ay;
    cxdouble by;

    cpl_matrix *coeffs;

};


inline static cxdouble
_giraffe_chebyshev2d_eval(const GiChebyshev2D *self, cxdouble x, cxdouble y)
{

    cxint i, k;
    cxint nx = self->xorder + 1;
    cxint ny = self->yorder + 1;

    cxdouble xn = (2.0 * x - self->ax - self->bx) / (self->bx - self->ax);
    cxdouble yn = (2.0 * y - self->ay - self->by) / (self->by - self->ay);
    cxdouble cx0 = 1.0;
    cxdouble cx1 = xn;
    cxdouble sum = 0.;
    cxdouble *_coeffs = cpl_matrix_get_data(self->coeffs);


    cx_assert(_coeffs != NULL);

    for (i = 0, k = 0; i < nx; i++) {

        register cxint j;

        register cxdouble cy0 = 1.0;
        register cxdouble cy1 = yn;
        register cxdouble cx2 = 0.;


        if (i < 2) {
            cx2 = cx0;
        }
        else {
            cx2 = 2.0 * cx1 * xn - cx0;
        }

        for (j = 0; j < ny; j++) {

            cxdouble cy2 = 0.;

            if (j < 2) {
                cy2 = cy0;
            }
            else {
                cy2 = 2.0 * cy1 * yn - cy0;
            }

            sum += cx2 * cy2 * _coeffs[k++];

            cy0 = cy1;
            cy1 = cy2;

        }

        cx0 = cx1;
        cx1 = cx2;

    }

    return sum;

}


GiChebyshev2D *
giraffe_chebyshev2d_new(cxint xorder, cxint yorder)
{

    GiChebyshev2D *self = cx_calloc(1, sizeof *self);


    if (self) {

        self->xorder = xorder;
        self->yorder = yorder;

        self->coeffs = cpl_matrix_new((xorder + 1), (yorder + 1));

        if (self->coeffs == NULL) {
            giraffe_chebyshev2d_delete(self);
            return NULL;
        }

    }

    return self;

}


GiChebyshev2D *
giraffe_chebyshev2d_clone(const GiChebyshev2D *other)
{

    GiChebyshev2D *self = NULL;


    if (other != NULL) {

        self = giraffe_chebyshev2d_new(other->xorder, other->yorder);

        self->ax = other->ax;
        self->bx = other->bx;
        self->ay = other->ay;
        self->by = other->by;

        self->coeffs = cpl_matrix_duplicate(other->coeffs);

    }

    return self;

}


void
giraffe_chebyshev2d_delete(GiChebyshev2D *self)
{

    if (self) {

        if (self->coeffs) {
            cpl_matrix_delete(self->coeffs);
            self->coeffs = NULL;
        }

        cx_free(self);

    }

    return;

}


void giraffe_chebyshev2d_get_order(const GiChebyshev2D *self, cxint *xorder,
                                   cxint *yorder)
{

    cx_assert(self != NULL);

    if (xorder != NULL) {
        *xorder = self->xorder;
    }

    if (yorder != NULL) {
        *yorder = self->yorder;
    }

    return;

}


void
giraffe_chebyshev2d_get_range(const GiChebyshev2D *self, cxdouble *ax,
                              cxdouble *bx, cxdouble *ay, cxdouble *by)
{

    cx_assert(self != NULL);

    if (ax != NULL) {
        *ax = self->ax;
    }

    if (bx != NULL) {
        *bx = self->bx;
    }

    if (ay != NULL) {
        *ay = self->ay;
    }

    if (by != NULL) {
        *by = self->by;
    }

    return;

}


const cpl_matrix *
giraffe_chebyshev2d_coeffs(const GiChebyshev2D *self)
{

    cx_assert(self != NULL);

    return self->coeffs;

}


cxint
giraffe_chebyshev2d_set(GiChebyshev2D *self, cxdouble ax, cxdouble bx,
                        cxdouble ay, cxdouble by, cpl_matrix *coeffs)
{

    cx_assert(self != NULL);

    self->ax = ax;
    self->bx = bx;
    self->ay = ay;
    self->by = by;

    if (cpl_matrix_get_nrow(coeffs) <= self->xorder ||
        cpl_matrix_get_ncol(coeffs) <= self->yorder) {
        return 1;
    }
    else {

        cxint i;

        for (i = 0; i <= self->xorder; i++) {

            cxint j;

            for (j = 0; j <= self->yorder; j++) {

                cxdouble c = cpl_matrix_get(coeffs, i, j);

                cpl_matrix_set(self->coeffs, i, j, c);

            }

        }

    }

    return 0;

}


cxint
giraffe_chebyshev2d_set_coeff(GiChebyshev2D *self, cxint i, cxint j,
                              cxdouble value)
{

    cx_assert(self != NULL);

    if (i > self->xorder || j > self->yorder) {
        return 1;
    }

    cpl_matrix_set(self->coeffs, i, j, value);

    return 0;

}


cxdouble
giraffe_chebyshev2d_eval(const GiChebyshev2D *self, cxdouble x, cxdouble y)
{

    cx_assert(self != NULL);
    return _giraffe_chebyshev2d_eval(self, x, y);

}
/**@}*/
