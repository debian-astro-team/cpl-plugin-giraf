/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIMATH_H
#define GIMATH_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_matrix.h>


#ifdef __cplusplus
extern "C" {
#endif


cxdouble
giraffe_interpolate_linear(cxdouble x, cxdouble x_0, cxdouble y_0,
                           cxdouble x_1, cxdouble y_1);

/*
 * Chebyshev polynomials
 */

cpl_matrix*
giraffe_chebyshev_base1d(cxdouble start, cxdouble size, cxint order,
                         cpl_matrix* m_x);

cpl_matrix*
giraffe_chebyshev_base2d(cxdouble xstart, cxdouble ystart, cxdouble xsize,
                         cxdouble ysize, cxint xorder, cxint yorder,
                         cpl_matrix* m_x, cpl_matrix* m_y);

cpl_matrix*
giraffe_chebyshev_base2dt(cxdouble xstart, cxdouble ystart, cxdouble xsize,
                          cxdouble ysize, cxint xorder, cxint yorder,
                          cpl_matrix* m_x, cpl_matrix* m_y);

cpl_matrix*
giraffe_chebyshev_fit1d(cxdouble start, cxdouble size, cpl_matrix* m_c,
                        cpl_matrix* m_x);

cpl_matrix*
giraffe_chebyshev_fit2d(cxdouble xstart, cxdouble ystart, cxdouble xsize,
                        cxdouble ysize, const cpl_matrix* m_c,
                        const cpl_matrix* m_x, const cpl_matrix* m_y);

cxint   giraffe_gauss_jordan(cpl_matrix*, cxint, cpl_matrix*, cxint);

void    giraffe_compute_image_coordinates(cxlong, cxlong, cpl_matrix*,
                                          cpl_matrix*);


#ifdef __cplusplus
}
#endif

#endif /* GIMATH_H */



