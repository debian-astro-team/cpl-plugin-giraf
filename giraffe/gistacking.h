/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GISTACKING_H
#define GISTACKING_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_frame.h>
#include <cpl_table.h>
#include <cpl_parameterlist.h>

#include "giimage.h"


#ifdef __cplusplus
extern "C" {
#endif


enum GiStackingMethod {
    GISTACKING_METHOD_UNDEFINED = 0,
    GISTACKING_METHOD_AVERAGE = 1,
    GISTACKING_METHOD_MEDIAN = 2,
    GISTACKING_METHOD_MINMAX = 3,
    GISTACKING_METHOD_KSIGMA = 4
};

typedef enum GiStackingMethod GiStackingMethod;

struct GiStackingConfig {
    GiStackingMethod stackmethod;
    cxdouble         ksigmalow;
    cxdouble         ksigmahigh;
    cxint            rejectmax;
    cxint            rejectmin;
    cxint            min_nr_frames;
};

typedef struct GiStackingConfig GiStackingConfig;


GiImage* giraffe_stacking_average(GiImage**, const GiStackingConfig*);
GiImage* giraffe_stacking_median(GiImage**, const GiStackingConfig*);
GiImage* giraffe_stacking_minmax(GiImage**, const GiStackingConfig*);
GiImage* giraffe_stacking_ksigma(GiImage**, const GiStackingConfig*);
GiImage* giraffe_stacking_stack_images(GiImage**, const GiStackingConfig*);
GiStackingConfig* giraffe_stacking_config_create(cpl_parameterlist*);

void giraffe_stacking_config_destroy(GiStackingConfig*);
void giraffe_stacking_config_add(cpl_parameterlist*);


#ifdef __cplusplus
}
#endif

#endif /* GISTACKING_H */
