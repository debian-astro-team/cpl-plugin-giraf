/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GISGCALIBRATION_H
#define GISGCALIBRATION_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_parameterlist.h>

#include <girange.h>
#include <giimage.h>
#include <gitable.h>
#include <gilocalization.h>
#include <giextraction.h>
#include <girebinning.h>


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief
 *   Slit geometry calibration configuration data structure.
 *
 * TBD
 */


struct GiSGCalConfig {

    /**
     * Number of slit geometry re-computations.
     */

    cxint repeat;

    /**
     * Intensity threshold. Pixel values larger than this threshold
     * are replaced by this value.
     */

    cxdouble zmax;

    /**
     * Step of the cross-correlation in nm.
     */

    cxdouble cc_step;

    /**
     * Flag, indicating whether the specified domain is given in pixels
     * or in nanometers.
     */

    cxbool cc_wdomain;

    /**
     * The limits of the spectral range, on which the cross-correlation
     * is made.
     */

    GiRange* cc_domain;

    /**
     * Delta RV limits of the cross-correlation window in km/s.
     */

    GiRange* rv_limits;

    /**
     * Maximum number of iteration used to determine the radial velocity RV.
     */

    cxint rv_niter;

    /**
     * Data window width factor.
     */

    cxdouble rv_wfactor;

    /**
     * Peak fit maximum number of iterations.
     */

    cxint pf_niter;

    /**
     * Peak fit maximum number of tests.
     */

    cxint pf_ntest;

    /**
     * Peak fit minimum delta chi-square.
     */

    cxdouble pf_dchisq;

};

typedef struct GiSGCalConfig GiSGCalConfig;


cxint giraffe_calibrate_slit(GiTable* result, const GiExtraction* extraction,
                             const GiLocalization* localization,
                             const GiTable* fibers, const GiTable* wlsolution,
                             const GiTable* slitgeometry,
                             const GiTable* grating, const GiTable* mask,
                             const GiSGCalConfig* config);

cxint giraffe_compute_offsets(GiTable* fibers,
                              const GiRebinning* rebinning,
                              const GiTable* grating,
                              const GiTable* mask,
                              const GiSGCalConfig* config);

GiSGCalConfig* giraffe_sgcalibration_config_create(cpl_parameterlist* list);
void giraffe_sgcalibration_config_destroy(GiSGCalConfig* config);
void giraffe_sgcalibration_config_add(cpl_parameterlist* list);


#ifdef __cplusplus
}
#endif

#endif /* GISGCALIBRATION_H */
