/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GILOCALIZATION_H
#define GILOCALIZATION_H

#include <giimage.h>
#include <gitable.h>
#include <gipsfdata.h>


#ifdef __cplusplus
extern "C" {
#endif

    struct GiLocalization {
        GiImage* locy;
        GiImage* locw;
        GiTable* locc;
        GiPsfData* psf;
    };

    typedef struct GiLocalization GiLocalization;

    GiLocalization* giraffe_localization_new(void);
    GiLocalization* giraffe_localization_create(GiImage* locy,
                                                GiImage* locw,
                                                GiTable* locc,
                                                GiPsfData* psf);
    void giraffe_localization_delete(GiLocalization* localization);
    void giraffe_localization_destroy(GiLocalization* localization);


#ifdef __cplusplus
}
#endif

#endif /* GILOCALIZATION_H */
