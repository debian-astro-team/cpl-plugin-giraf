/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIPSF_H
#define GIPSF_H

#include <cpl_macros.h>
#include <cpl_parameterlist.h>

#include <giimage.h>
#include <gipsfdata.h>
#include <gimodel.h>
#include <giclip.h>


#ifdef __cplusplus
extern "C" {
#endif

    struct GiPsfConfig {
        const cxchar* profile;
        cxint         binsize;
        cxint         maxwidth;
        cxint         width;
        cxint         exponent;
        cxint         yorder;
        cxint         worder;
        cxbool        normalize;
        cxbool        parameter_fit;
        GiFitSetup    fit;
        GiClipParams  clip;
    };

    typedef struct GiPsfConfig GiPsfConfig;

    cxint giraffe_compute_fiber_profiles(GiLocalization* result,
                                         GiImage* image,
                                         GiTable* fibers,
                                         GiLocalization* master,
                                         GiImage* bpixel,
                                         GiPsfConfig* config);

    /*
     * Convenience functions
     */

    GiPsfConfig* giraffe_psf_config_create(cpl_parameterlist* list);
    void giraffe_psf_config_destroy(GiPsfConfig* self);

    void giraffe_psf_config_add(cpl_parameterlist* list);


#ifdef __cplusplus
}
#endif

#endif /* GIPSF_H */
