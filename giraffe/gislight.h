/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GISLIGHT_H
#define GISLIGHT_H

#include <cxtypes.h>

#include <cpl_parameterlist.h>

#include <giimage.h>
#include <gilocalization.h>


#ifdef __cplusplus
extern "C" {
#endif

    struct GiSLightConfig {
        const cxchar* model;
        cxint xorder[2];
        cxint yorder[2];
        cxint xstep;
        cxint ystep;
        cxdouble ewidth;
        cxint iswidth;
        cxbool istrim;
        cxbool phffcor;
        cxbool remove;
    };

    typedef struct GiSLightConfig GiSLightConfig;


    cxint giraffe_adjust_scattered_light(GiImage* result,
                                         const GiImage* image,
                                         const GiLocalization* localization,
                                         const GiImage* bpixel, GiImage* phff,
                                         const GiSLightConfig* config);

    GiSLightConfig* giraffe_slight_config_create(cpl_parameterlist*);
    void giraffe_slight_config_destroy(GiSLightConfig*);
    void giraffe_slight_config_add(cpl_parameterlist*);

#ifdef __cplusplus
}
#endif

#endif /* GISLIGHT_H */
