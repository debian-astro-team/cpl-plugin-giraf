/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 *  This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIMODEL_H
#define GIMODEL_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_matrix.h>


#ifdef __cplusplus
extern "C" {
#endif


    enum GiModelType {
        GI_MODEL_LINE = 1,
        GI_MODEL_XOPT = 2,
        GI_MODEL_YOPT = 3,
        GI_MODEL_LOCY = 4
    };

    typedef enum GiModelType GiModelType;

    typedef struct GiModel GiModel;

    struct GiFitSetup {

        cxint iterations;
        cxint tests;
        cxdouble delta;

    };

    typedef struct GiFitSetup GiFitSetup;


    GiModel* giraffe_model_new(const cxchar* name);
    GiModel* giraffe_model_clone(const GiModel* other);
    void giraffe_model_delete(GiModel* self);

    const cxchar* giraffe_model_get_name(const GiModel* self);
    GiModelType giraffe_model_get_type(const GiModel* self);

    cxsize giraffe_model_count_arguments(const GiModel* self);
    cxsize giraffe_model_count_parameters(const GiModel* self);

    const cxchar* giraffe_model_argument_name(const GiModel* self,
                                            cxsize position);
    const cxchar* giraffe_model_parameter_name(const GiModel* self,
                                            cxsize position);

    cxint giraffe_model_set_argument(GiModel* self, const cxchar* name,
                                    cxdouble value);
    cxdouble giraffe_model_get_argument(const GiModel* self,
                                        const cxchar* name);

    cxint giraffe_model_set_parameter(GiModel* self, const cxchar* name,
                                    cxdouble value);
    cxdouble giraffe_model_get_parameter(const GiModel* self,
                                        const cxchar* name);

    cxint giraffe_model_freeze_parameter(GiModel* self, const cxchar* name);
    cxint giraffe_model_thaw_parameter(GiModel* self, const cxchar* name);

    cxbool giraffe_model_frozen_parameter(const GiModel* self,
                                        const cxchar* name);

    cxint giraffe_model_freeze(GiModel* self);
    cxint giraffe_model_thaw(GiModel* self);

    cxint giraffe_model_evaluate(const GiModel* self, cxdouble* result,
                                cxint* status);

    cxint giraffe_model_fit(GiModel* self, cpl_matrix* x, cpl_matrix* y,
                            cpl_matrix* sigma);
    cxint giraffe_model_fit_sequence(GiModel* self, cpl_matrix* x,
                                     cpl_matrix* y, cpl_matrix* sigma,
                                     cxint ndata, cxint start, cxint stride);

    cxint giraffe_model_set_iterations(GiModel* self, cxint iterations);
    cxint giraffe_model_get_iterations(const GiModel* self);

    cxint giraffe_model_set_delta(GiModel* self, cxdouble delta);
    cxdouble giraffe_model_get_delta(const GiModel* self);

    cxint giraffe_model_set_tests(GiModel* self, cxint tests);
    cxint giraffe_model_get_tests(const GiModel* self);

    cxint giraffe_model_get_position(const GiModel* self);
    cxint giraffe_model_get_df(const GiModel* self);
    cxdouble giraffe_model_get_chisq(const GiModel* self);
    cxdouble giraffe_model_get_rsquare(const GiModel* self);

    cxdouble giraffe_model_get_variance(const GiModel* self,
                                        const cxchar* name);
    cxdouble giraffe_model_get_sigma(const GiModel* self,
                                     const cxchar* name);


#ifdef __cplusplus
}
#endif

#endif /* GIMODEL_H */
