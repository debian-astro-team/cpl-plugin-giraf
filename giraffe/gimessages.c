/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxmessages.h>

#include "gimessages.h"


/**
 * @defgroup gimessages Error Reporting Utilities
 *
 * TBD
 */

/**@{*/

/**
 * @brief
 *   Log an error message.
 *
 * @param format  The format string.
 * @param ...     Arguments to be inserted into the format string.
 *
 * @return Nothing.
 *
 * This is a convenience function to log an error message specified by the
 * format string @em format and the following list of arguments via the
 * installed log handler.
 *
 * Error messages are always considered fatal, i.e. the application is
 * immediately terminated by a call to @b abort() causing a core dump. Do
 * not use this function for expected (recoverable) errors.
 * This function should be used to indicate a bug (assertion failure) in the
 * application.
 *
 * @see gi_critical
 */

void gi_error(const cxchar *format, ...)
{

    va_list args;

    va_start(args, format);
    cx_logv(CX_LOG_DOMAIN, CX_LOG_LEVEL_ERROR, format, args);
    va_end(args);

    return;
}

/**
 * @brief
 *   Log a "critical" warning.
 *
 * @param format  The format string.
 * @param ...     Arguments to be inserted into the format string.
 *
 * @return Nothing.
 *
 * This is a convenience function to log a message with level
 * @c CX_LOG_LEVEL_CRITICAL, as specified by the format string @em format and
 * the following list of arguments, via the installed log handler.
 *
 * It is up to the application to decide which warnings are critical and
 * which are not. To cause a termination of the application on critical
 * warnings you may call @b cx_log_set_always_fatal().
 *
 * @see gi_warning
 */

void gi_critical(const cxchar *format, ...)
{
    va_list args;

    va_start(args, format);
    cx_logv(CX_LOG_DOMAIN, CX_LOG_LEVEL_CRITICAL, format, args);
    va_end(args);

    return;
}


/**
 * @brief
 *   Log a warning.
 *
 * @param format  The format string.
 * @param ...     Arguments to be inserted into the format string.
 *
 * @return Nothing.
 *
 * This is a convenience function to log a warning message, as specified
 * by the format string @em format and the following list of arguments,
 * via the installed log handler.
 *
 * @see gi_critical
 */

void gi_warning(const cxchar *format, ...)
{
    va_list args;

    va_start(args, format);
    cx_logv(CX_LOG_DOMAIN, CX_LOG_LEVEL_WARNING, format, args);
    va_end(args);

    return;
}


/**
 * @brief
 *   Log a normal message.
 *
 * @param format  The format string.
 * @param ...     Arguments to be inserted into the format string.
 *
 * @return Nothing.
 *
 * This is a convenience function to log an ordinary message, as specified
 * by the format string @em format and the following list of arguments,
 * via the installed log handler.
 *
 */

void gi_message(const cxchar *format, ...)
{
    va_list args;

    va_start(args, format);
    cx_logv(CX_LOG_DOMAIN, CX_LOG_LEVEL_MESSAGE, format, args);
    va_end(args);

    return;
}
/**@}*/
