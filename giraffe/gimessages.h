/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIMESSAGES_H
#define GIMESSAGES_H

#include <stdarg.h>

#include <cxtypes.h>

#include <cpl_macros.h>


#ifdef __cplusplus
extern "C" {
#endif

/*
 * Macro to allow the compiler to type-check the arguments of printf like
 * functions. The arguments of the macro are the position of the format and
 * the first argument to be checked within the argument list of the function.
 * The numbering of the arguments of the function start from one.
 *
 * It is only made available if gcc is used!
 */

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ > 4)
#  define GI_GNUC_PRINTF(format_idx, arg_idx) \
	__attribute__((format(__printf__, (format_idx), (arg_idx))))
#else  /* !GNUC */
#  define GI_GNUC_PRINTF(format_idx, arg_idx)
#endif /* !GNUC */


#if !defined CX_LOG_DOMAIN
#  define CX_LOG_DOMAIN  "GiraffeLib"
#endif


void gi_error(const cxchar *, ...) GI_GNUC_PRINTF(1, 2);
void gi_critical(const cxchar *, ...) GI_GNUC_PRINTF(1, 2);
void gi_warning(const cxchar *, ...) GI_GNUC_PRINTF(1, 2);
void gi_message(const cxchar *, ...) GI_GNUC_PRINTF(1, 2);


#ifdef __cplusplus
}
#endif

#endif /* GIMESSAGES_H */
