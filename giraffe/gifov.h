/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIFOV_H
#define GIFOV_H

#include <cxslist.h>

#include <cpl_macros.h>
#include <cpl_array.h>
#include <cpl_parameterlist.h>

#include <giimage.h>
#include <gitable.h>
#include <gicube.h>
#include <girebinning.h>


#ifdef __cplusplus
extern "C" {
#endif


    enum GiFieldOfViewCubeFormat {
        GIFOV_FORMAT_SINGLE = 1 << 0,
        GIFOV_FORMAT_ESO3D  = 1 << 1
    };

    typedef enum GiFieldOfViewCubeFormat GiFieldOfViewCubeFormat;


    struct GiFieldOfViewConfig {
        cxdouble minimum;
        cxdouble maximum;
        cxbool cube;

        GiFieldOfViewCubeFormat format;
    };

    typedef struct GiFieldOfViewConfig GiFieldOfViewConfig;


    struct GiFieldOfView {
        GiInstrumentMode mode;

        cpl_array* ssn;

        struct {
            GiImage* spectra;
            GiImage* errors;
        } fov;

        struct {
            cx_slist* spectra;
            cx_slist* errors;
        } images;

        struct {
            cx_slist* spectra;
            cx_slist* errors;
        } cubes;
    };

    typedef struct GiFieldOfView GiFieldOfView;


    /*
     * Spectrum localization
     */

    cxint giraffe_fov_build(GiFieldOfView* result,
                            GiRebinning* rebinning, GiTable* fibers,
                            GiTable* wsolution, GiTable* grating,
                            GiTable* slitgeometry,
                            GiFieldOfViewConfig* config);


    /*
     * Convenience functions
     */

    GiFieldOfView* giraffe_fov_new(void);
    void giraffe_fov_delete(GiFieldOfView* self);
    void giraffe_fov_clear(GiFieldOfView* self);

    cxint giraffe_fov_save_cubes(const GiFieldOfView* self,
                                 cpl_propertylist* properties,
                                 const cxchar* filename, cxptr data);

    cxint giraffe_fov_save_cubes_eso3d(const GiFieldOfView* self,
                                       cpl_propertylist* properties,
                                       const cxchar* filename, cxptr data);

    GiFieldOfViewConfig* giraffe_fov_config_create(cpl_parameterlist* list);
    void giraffe_fov_config_destroy(GiFieldOfViewConfig* config);
    void giraffe_fov_config_add(cpl_parameterlist* list);


#ifdef __cplusplus
}
#endif

#endif /* GIFOV_H */
