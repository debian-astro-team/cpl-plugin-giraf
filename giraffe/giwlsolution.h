/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIWLSOLUTION_H
#define GIWLSOLUTION_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_table.h>

#include <gimodel.h>
#include <gigrating.h>
#include <giwlresiduals.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef struct GiWlSolution GiWlSolution;


GiWlSolution *giraffe_wlsolution_new(const cxchar *name, cxint orientation,
                                     cxint npixels, cxdouble pixelsize,
                                     GiGrating *grating);
GiWlSolution *giraffe_wlsolution_clone(const GiWlSolution *other);
GiWlSolution *giraffe_wlsolution_create(GiTable *solution, GiImage *spectra,
                                        GiGrating *grating);
void giraffe_wlsolution_delete(GiWlSolution *self);

const cxchar *giraffe_wlsolution_name(const GiWlSolution *self);
GiModel *giraffe_wlsolution_model(const GiWlSolution *self);

void giraffe_wlsolution_reset_residuals(GiWlSolution *self);

cxint giraffe_wlsolution_set_subslits(GiWlSolution *self, cxbool flag);
cxbool giraffe_wlsolution_get_subslits(const GiWlSolution *self);

cxint giraffe_wlsolution_set_residuals(GiWlSolution *self,
                                       const GiWlResiduals *residuals);
GiWlResiduals *giraffe_wlsolution_get_residuals(const GiWlSolution *self);

cxdouble giraffe_wlsolution_compute_pixel(const GiWlSolution *self,
                                          cxdouble lambda, cxdouble x,
                                          cxdouble y, cxint *status);

cxdouble giraffe_wlsolution_compute_residual(const GiWlSolution *self,
                                             cxdouble x, cxdouble y);

GiTable *giraffe_wlsolution_create_table(const GiWlSolution *solution);


#ifdef __cplusplus
}
#endif

#endif /* GIWLSOLUTION_H */
