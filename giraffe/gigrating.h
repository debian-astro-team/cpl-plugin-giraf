/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIGRATING_H
#define GIGRATING_H

#include <cxstring.h>

#include <cpl_macros.h>
#include <cpl_msg.h>

#include <giimage.h>
#include <gitable.h>


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Structure to handle Grating Information
 *
 * Structure stores the relevant characteristics of a specific
 * Giraffe Grating
 */

struct GiGrating {
    cx_string *setup;       /**< grating setup name              */
    cx_string *name;        /**< grating name e.g. HR            */
    cx_string *filter;      /**< grating filter name e.g. HR13   */
    cx_string *slit;        /**< grating slit name e.g. Medusa1  */
    cxint     order;        /**< grating diffraction order       */
    cxdouble  wlen0;        /**< grating central wavelength {mm} */
    cxdouble  wlenmin;      /**< grating minimum wavelength {mm} */
    cxdouble  wlenmax;      /**< grating maximum wavelength {mm} */
    cxdouble  band;         /**< grating band                    */
    cxint     resol;        /**< grating resolution              */
    cxdouble  space;        /**< grating groove spacing {mm}     */
    cxdouble  theta;        /**< grating angle {rad}             */
    cxdouble  fcoll;        /**< collimator focal length {mm}    */
    cxdouble  gcam;         /**< camera magnification factor     */
    cxdouble  sdx;          /**< slit position X offset {mm}     */
    cxdouble  sdy;          /**< slit position Y offset {mm}     */
    cxdouble  sphi;         /**< slit position angle {rad}       */
};

typedef struct GiGrating GiGrating;

GiGrating *giraffe_grating_new(void);
GiGrating *giraffe_grating_create(const GiImage *spectra,
                                  const GiTable *grating);
void giraffe_grating_delete(GiGrating *);

void giraffe_grating_dump(const GiGrating *);

cxint giraffe_grating_setup(GiTable *grating_table, GiImage *spectra,
                            GiGrating *grating_setup);


#ifdef __cplusplus
}
#endif

#endif /* GIGRATING_H */
