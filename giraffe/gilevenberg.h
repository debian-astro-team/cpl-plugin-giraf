/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GILEVENBERG_H
#define GILEVENBERG_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_matrix.h>


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief
 *   Non-linear fit control parameters
 */

struct GiFitParams {

    /**
     * Maximum number of iterations
     */

    cxint iterations;

    /**
     * Maximum number of chi-square tests
     */

    cxint tests;

    /**
     * Minimum delta chi-square, indicating convergence
     */
    cxdouble dchisq;

};

typedef struct GiFitParams GiFitParams;


/**
 * @brief
 *   Fit function type definition.
 */

typedef void (*GiFitFunc)(cxdouble *result, cxdouble *x, cxdouble *a,
                          cxint na, cxdouble *dyda, cxdouble *r);


cxint giraffe_nlfit(cpl_matrix *x, cpl_matrix *y, cpl_matrix *sigma,
                    cxint ndata, cpl_matrix *a, cpl_matrix *delta,
                    cxint *ia, cxint ma, cpl_matrix *alpha, cxdouble *chisq,
                    GiFitFunc funcs, const GiFitParams *setup);


#ifdef __cplusplus
}
#endif

#endif /* GILEVENBERG_H */
