/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIPAF_H
#define GIPAF_H

#include <cpl_macros.h>
#include <cpl_propertylist.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef struct GiPaf GiPaf;

GiPaf *giraffe_paf_new(const cxchar *name, const cxchar *type,
                       const cxchar *id, const cxchar *description);
void giraffe_paf_delete(GiPaf *self);

cxchar *giraffe_paf_get_name(const GiPaf *self);
cxint giraffe_paf_set_name(GiPaf *self, const cxchar *name);

cxchar *giraffe_paf_get_type(const GiPaf *self);
cxint giraffe_paf_set_type(GiPaf *self, const cxchar *type);

cxchar *giraffe_paf_get_id(const GiPaf *self);
cxint giraffe_paf_set_id(GiPaf *self, const cxchar *id);

cxchar *giraffe_paf_get_description(const GiPaf *self);
cxint giraffe_paf_set_description(GiPaf *self, const cxchar *description);

cpl_propertylist *giraffe_paf_get_properties(const GiPaf *self);
cxint giraffe_paf_set_properties(GiPaf *self,
                                 const cpl_propertylist *properties);

cxint giraffe_paf_write(const GiPaf *self);


#ifdef __cplusplus
}
#endif

#endif /* GIPAF_H */
