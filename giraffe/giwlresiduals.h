/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIWLRESIDUALS_H
#define GIWLRESIDUALS_H

#include <cxtypes.h>

#include <cpl_macros.h>

#include <gitable.h>
#include <gichebyshev.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef struct GiWlResiduals GiWlResiduals;


GiWlResiduals *giraffe_wlresiduals_new(void);
GiWlResiduals *giraffe_wlresiduals_clone(const GiWlResiduals *other);
GiWlResiduals *giraffe_wlresiduals_create(GiTable *wlsolution);
void giraffe_wlresiduals_delete(GiWlResiduals *self);

cxsize giraffe_wlresiduals_get_size(const GiWlResiduals *self);

cxint giraffe_wlresiduals_get_subslit(const GiWlResiduals *self, cxsize idx);
GiChebyshev2D *giraffe_wlresiduals_get_element(const GiWlResiduals *self,
                                               cxsize idx);

cxint giraffe_wlresiduals_set(GiWlResiduals *self, cxint ssn,
                              const GiChebyshev2D *residuals);
GiChebyshev2D *giraffe_wlresiduals_get(const GiWlResiduals *self, cxint ssn);

cpl_table *giraffe_wlresiduals_table(const GiWlResiduals *self);


#ifdef __cplusplus
}
#endif

#endif /* GIWLRESIDUALS_H */
