/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIMODELS_H
#define GIMODELS_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_propertylist.h>

#include "gimodel.h"
#include "gilevenberg.h"


#ifdef __cplusplus
extern "C" {
#endif


struct GiModel {

    cxchar* name;
    GiModelType type;

    GiFitFunc model;

    struct {
        cxint count;
        cpl_propertylist* names;
        cpl_matrix* values;
    } arguments;

    struct {
        cxint count;
        cpl_propertylist* names;
        cpl_matrix* values;
        cpl_matrix* limits;
        cxint* flags;
    } parameters;

    struct {
        GiFitSetup setup;

        cxint iterations;
        cxint nfree;
        cxint df;

        cxdouble chisq;
        cxdouble rsquare;

        cpl_matrix* covariance;
    } fit;

};


typedef struct GiModelData GiModelData;

struct GiModelData {

    const cxchar* const name;

    GiModelType type;

    void (*ctor)(GiModel* , const GiModelData* );
    void (*dtor)(GiModel* );
    GiFitFunc eval;

};


extern const GiModelData* const giraffe_models;


#ifdef __cplusplus
}
#endif

#endif /* GIMODELS_H */
