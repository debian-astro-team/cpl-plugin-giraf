/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxmemory.h>
#include <cxstring.h>
#include <cxstrutils.h>

#include <cpl_error.h>
#include <cpl_errorstate.h>

#include "gierror.h"


struct GiError {
    cpl_errorstate state;
};

typedef struct GiError GiError;


static GiError _error ;

static cxbool _error_init = FALSE;
static cxbool _error_saved = FALSE;


/**
 * @defgroup gierror Error Handling Utilities
 *
 * TBD
 */

/**@{*/

void
giraffe_error_init(void)
{

    _error_init = TRUE;
    return;

}


void
giraffe_error_clear(void)
{

    if (_error_init == TRUE) {
        _error_init = FALSE;
    }

    return;

}


void
giraffe_error_push(void)
{

    cx_assert(_error_init == TRUE);

    _error.state = cpl_errorstate_get();
    _error_saved = TRUE;

    cpl_error_reset();

    return;

}


void
giraffe_error_pop(void)
{

    cx_assert(_error_init == TRUE);

    if (_error_saved == TRUE) {

        cpl_errorstate_set(_error.state);
        _error_saved = FALSE;

    }

    return;

}
/**@}*/
