/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIDEBUG_H
#define GIDEBUG_H

#include <cpl_macros.h>


#ifdef __cplusplus
extern "C" {
#endif


#ifdef GI_ENABLE_DEBUG
#  define GIDEBUG(action)  do {            \
                               action;     \
                           } while (0)
#else /* !GI_ENABLE_DEBUG */
#  define GIDEBUG(action)
#endif /* GI_ENABLE_DEBUG */


#ifdef __cplusplus
}
#endif

#endif /* GIDEBUG_H */
