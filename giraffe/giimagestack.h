/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIIMAGESTACK_H
#define GIIMAGESTACK_H

#include <cxmacros.h>
#include <cxtypes.h>

#include <cpl_image.h>


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief
 *   Structure to handle an array/stack of cpl_images
 *
 * Structure stores a pointer to an array of cpl_images
 * and the (maximum) number of images that can be/are stored in it.
 */

struct GiImageStack {

    cxint       nimages;  /**< (maximum) number of images in array  */
    cpl_image **images;   /**< pointer to array of cpl_images       */

};

typedef struct GiImageStack GiImageStack;

GiImageStack *giraffe_imagestack_new(cxint);
void giraffe_imagestack_delete(GiImageStack *);
cxint giraffe_imagestack_set(GiImageStack *, cxint, cpl_image *);
cpl_image *giraffe_imagestack_get(GiImageStack *, cxint);
cxint giraffe_imagestack_size(GiImageStack *);
cxint giraffe_imagestack_resize(GiImageStack *istack, cxint size);


#ifdef __cplusplus
}
#endif

#endif /* GIIMAGESTACK_H */
