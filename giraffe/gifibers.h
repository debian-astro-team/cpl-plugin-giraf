/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIFIBERS_H
#define GIFIBERS_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_frame.h>
#include <cpl_parameterlist.h>

#include <gitable.h>


#ifdef __cplusplus
extern "C" {
#endif


struct GiFibersConfig {
    cxint nspec;
    cxchar *spectra;
};

typedef struct GiFibersConfig GiFibersConfig;


/*
 * Fiber selection
 */

GiTable *giraffe_fibers_select(const cpl_frame *frame,
                               const GiTable *reference,
                               GiFibersConfig *config);

GiTable *giraffe_fibers_setup(const cpl_frame *frame,
                              const cpl_frame *reference);


/*
 * Convenience functions
 */

GiFibersConfig *giraffe_fibers_config_create(cpl_parameterlist *list);
void giraffe_fibers_config_destroy(GiFibersConfig *config);


void giraffe_fibers_config_add(cpl_parameterlist *list);


#ifdef __cplusplus
}
#endif

#endif /* GIFIBERS_H */
