/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIFIBERUTILS_H
#define GIFIBERUTILS_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_array.h>
#include <cpl_table.h>
#include <cpl_frame.h>

#include <gitable.h>


#ifdef __cplusplus
extern "C" {
#endif


#define GIOZPOZ_EXTENSION  (1)
#define GIOZPOZ_MAGIC      "OzPoz_table"

#define GIFIBER_EXTENSION  (2)
#define GIFIBER_MAGIC      "FLAMES FIBRE Table"


/*
 * Fiber table utilities
 */

cpl_table *giraffe_fiberlist_create(const cxchar *filename, cxint nspec,
                                    const cxint *spectra);

GiTable *giraffe_fiberlist_load(const cxchar *filename, cxint dataset,
                                const cxchar *tag);

cxint giraffe_fiberlist_save(GiTable *, const cxchar *filename);
cxint giraffe_fiberlist_attach(cpl_frame *frame, GiTable *fibers);

cxint giraffe_fiberlist_compare(const GiTable *fibers,
                                const GiTable *reference);

cxint giraffe_fiberlist_associate(GiTable *fibers, const GiTable *reference);

cxint giraffe_fiberlist_clear_index(GiTable* fibers);
const cxchar *giraffe_fiberlist_query_index(const cpl_table *fibers);

cpl_array* giraffe_fiberlist_get_subslits(const cpl_table* fibers);

cxint *giraffe_parse_spectrum_selection(const cxchar *selection,
                                        cxint *nspec);

cxint *giraffe_create_spectrum_selection(const cxchar *filename,
                                         const GiTable *reference,
                                         cxint *nspec);



#ifdef __cplusplus
}
#endif

#endif /* GIFIBERUTILS_H */
