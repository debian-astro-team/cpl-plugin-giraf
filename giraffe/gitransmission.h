/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GITRANSMISSION_H
#define GITRANSMISSION_H

#include <cxtypes.h>

#include <cpl_macros.h>

#include <giimage.h>
#include <gitable.h>
#include <gilocalization.h>
#include <giextraction.h>


#ifdef __cplusplus
extern "C" {
#endif


    struct GiTransmissionConfig {
        cxbool transmission;
    };

    typedef struct GiTransmissionConfig GiTransmissionConfig;


    cxint
    giraffe_transmission_compute(GiExtraction* extraction, GiTable* fibers,
                                 GiLocalization* localization,
                                 GiTable* wcalcoeff, GiTable* grating,
                                 GiTable* slitgeometry);

    cxint
    giraffe_transmission_setup(GiTable* fibers, GiTable* reference);

    cxint
    giraffe_transmission_apply(GiExtraction* spectra, GiTable* fibers);

    cxint
    giraffe_transmission_attach(GiTable* fibers, const cxchar* filename);


    /*
     * Convenience functions
     */

    GiTransmissionConfig*
    giraffe_transmission_config_create(cpl_parameterlist* list);

    void
    giraffe_transmission_config_destroy(GiTransmissionConfig* config);

    void
    giraffe_transmission_config_add(cpl_parameterlist* list);


#ifdef __cplusplus
}
#endif

#endif /* GITRANSMISSION_H */
