/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GITABLE_H
#define GITABLE_H

#include <cxtypes.h>

#include <cpl_macros.h>
#include <cpl_table.h>
#include <cpl_matrix.h>
#include <cpl_propertylist.h>
#include <cpl_frameset.h>

#include <giutils.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef struct GiTable GiTable;


/*
 * Create, copy and destroy operations
 */

GiTable *giraffe_table_new(void);
GiTable *giraffe_table_create(cpl_table *, cpl_propertylist *);
void giraffe_table_delete(GiTable *);
GiTable *giraffe_table_duplicate(const GiTable *);

void giraffe_table_clear(GiTable *);

cxint giraffe_table_copy_matrix(GiTable *, const cxchar *, cpl_matrix *);

/*
 * Non-modifying operations
 */

cxint giraffe_table_is_empty(GiTable *);

/*
 * Accessors
 */

cpl_table *giraffe_table_get(const GiTable *);
cxint      giraffe_table_set(GiTable *, cpl_table *);
cpl_propertylist *giraffe_table_get_properties(const GiTable *);
cxint      giraffe_table_set_properties(GiTable *, cpl_propertylist *);

/*
 * Loading and saving operations
 */

cxint giraffe_table_load(GiTable *self, const cxchar *filename,
                         cxint position, const cxchar *id);

cxint giraffe_table_save(GiTable *self, const cxchar *filename);

cxint giraffe_table_attach(GiTable *self, const cxchar *filename,
                           cxint position, const cxchar *id);

cxint giraffe_table_add_info(GiTable *, const GiRecipeInfo *,
                             const cpl_frameset *);


#ifdef __cplusplus
}
#endif

#endif /* GITABLE_H */
