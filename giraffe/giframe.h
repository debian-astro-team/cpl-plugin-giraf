/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIFRAME_H
#define GIFRAME_H

#include <cpl_macros.h>
#include <cpl_propertylist.h>
#include <cpl_frame.h>
#include <cpl_frameset.h>

#include <giimage.h>
#include <gitable.h>


#ifdef __cplusplus
extern "C" {
#endif

/*
 * Technical frames
 */

#define GIFRAME_LINEARITY_FLAT "LINEARITY_FLAT"
#define GIFRAME_LINEARITY_BIAS "LINEARITY_BIAS"


/*
 * Raw frames
 */

#define GIFRAME_BIAS          "BIAS"
#define GIFRAME_DARK          "DARK"
#define GIFRAME_FIBER_FLAT    "FIBER_FLAT"
#define GIFRAME_ARC_SPECTRUM  "ARC_SPECTRUM"
#define GIFRAME_SCIENCE       "SCIENCE"
#define GIFRAME_STANDARD      "STD"

/*
 * Calibration data
 */

#define GIFRAME_BADPIXEL_MAP   "BAD_PIXEL_MAP"
#define GIFRAME_GRATING        "GRATING_DATA"
#define GIFRAME_SLITGEOMETRY   "SLIT_GEOMETRY"
#define GIFRAME_SLITMASTER     "SLIT_GEOMETRY_MASTER"
#define GIFRAME_SLITSETUP      "SLIT_GEOMETRY_SETUP"
#define GIFRAME_LINE_MASK      "LINE_MASK"
#define GIFRAME_LINE_CATALOG   "LINE_CATALOG"
#define GIFRAME_EXTINCTION     "ATMOSPHERIC_EXTINCTION"
#define GIFRAME_FLUX_STANDARDS "FLUX_STANDARDS"

/*
 * Products
 */

#define GIFRAME_BIAS_MASTER              "MASTER_BIAS"
#define GIFRAME_DARK_MASTER              "MASTER_DARK"

#define GIFRAME_FIBER_FLAT_MASTER        "MASTER_FIBER_FLAT"
#define GIFRAME_FIBER_FLAT_EXTSPECTRA    "FF_EXTSPECTRA"
#define GIFRAME_FIBER_FLAT_EXTERRORS     "FF_EXTERRORS"
#define GIFRAME_FIBER_FLAT_EXTPIXELS     "FF_EXTPIXELS"
#define GIFRAME_FIBER_FLAT_EXTTRACE      "FF_EXTTRACES"
#define GIFRAME_FIBER_FLAT_EXTMODEL      "FF_EXTMODEL"

#define GIFRAME_LOCALIZATION_CENTROID    "FF_LOCCENTROID"
#define GIFRAME_LOCALIZATION_WIDTH       "FF_LOCWIDTH"
#define GIFRAME_LOCALIZATION_FIT         "FF_LOCFIT"

#define GIFRAME_PSF_CENTROID             "FF_PSFCENTROID"
#define GIFRAME_PSF_WIDTH                "FF_PSFWIDTH"
#define GIFRAME_PSF_FIT                  "FF_PSFFIT"

#define GIFRAME_ARC_LAMP_EXTSPECTRA      "ARC_EXTSPECTRA"
#define GIFRAME_ARC_LAMP_EXTERRORS       "ARC_EXTERRORS"
#define GIFRAME_ARC_LAMP_EXTPIXELS       "ARC_EXTPIXELS"
#define GIFRAME_ARC_LAMP_EXTTRACE        "ARC_EXTTRACES"
#define GIFRAME_ARC_LAMP_EXTMODEL        "ARC_EXTMODEL"
#define GIFRAME_ARC_LAMP_RBNSPECTRA      "ARC_RBNSPECTRA"
#define GIFRAME_ARC_LAMP_RBNERRORS       "ARC_RBNERRORS"

#define GIFRAME_WAVELENGTH_SOLUTION      "DISPERSION_SOLUTION"
#define GIFRAME_LINE_DATA                "LINE_DATA"
#define GIFRAME_PSF_DATA                 "FIBER_PROFILE"

#define GIFRAME_SCIENCE_REDUCED          "SCIENCE_REDUCED"
#define GIFRAME_SCIENCE_EXTSPECTRA       "SCIENCE_EXTSPECTRA"
#define GIFRAME_SCIENCE_EXTERRORS        "SCIENCE_EXTERRORS"
#define GIFRAME_SCIENCE_EXTPIXELS        "SCIENCE_EXTPIXELS"
#define GIFRAME_SCIENCE_EXTTRACE         "SCIENCE_EXTTRACES"
#define GIFRAME_SCIENCE_EXTMODEL         "SCIENCE_EXTMODEL"
#define GIFRAME_SCIENCE_RBNSPECTRA       "SCIENCE_RBNSPECTRA"
#define GIFRAME_SCIENCE_RBNERRORS        "SCIENCE_RBNERRORS"
#define GIFRAME_SCIENCE_RCSPECTRA        "SCIENCE_RCSPECTRA"
#define GIFRAME_SCIENCE_RCERRORS         "SCIENCE_RCERRORS"
#define GIFRAME_SCIENCE_CUBE_SPECTRA     "SCIENCE_CUBE_SPECTRA"
#define GIFRAME_SCIENCE_CUBE_ERRORS      "SCIENCE_CUBE_ERRORS"
#define GIFRAME_SCIENCE_CUBE             "SCIENCE_CUBE"

#define GIFRAME_STANDARD_REDUCED         "STD_REDUCED"
#define GIFRAME_STANDARD_EXTSPECTRA      "STD_EXTSPECTRA"
#define GIFRAME_STANDARD_EXTERRORS       "STD_EXTERRORS"
#define GIFRAME_STANDARD_EXTPIXELS       "STD_EXTPIXELS"
#define GIFRAME_STANDARD_EXTTRACE        "STD_EXTTRACES"
#define GIFRAME_STANDARD_EXTMODEL        "STD_EXTMODEL"
#define GIFRAME_STANDARD_RBNSPECTRA      "STD_RBNSPECTRA"
#define GIFRAME_STANDARD_RBNERRORS       "STD_RBNERRORS"
#define GIFRAME_STANDARD_RCSPECTRA       "STD_RCSPECTRA"
#define GIFRAME_STANDARD_RCERRORS        "STD_RCERRORS"
#define GIFRAME_STANDARD_CUBE_SPECTRA    "STD_CUBE_SPECTRA"
#define GIFRAME_STANDARD_CUBE_ERRORS     "STD_CUBE_ERRORS"
#define GIFRAME_STANDARD_CUBE            "STD_CUBE"

#define GIFRAME_SCATTERED_LIGHT_MODEL    "SCATTERED_LIGHT_MODEL"

#define GIFRAME_INSTRUMENT_RESPONSE      "INSTRUMENT_RESPONSE"
#define GIFRAME_EFFICIENCY_CURVE         "EFFICIENCY_CURVE"


/*
 * Miscellaneous
 */

#define GIFRAME_FIBER_SETUP  "FIBER_SETUP"


/*
 * Generic frame creation utilities
 */

typedef cxint (*GiFrameCreator)(cxcptr, cpl_propertylist*, const cxchar*,
                                cxcptr);

cpl_frame* giraffe_frame_create(const cxchar* tag, cpl_frame_level level,
                                const cpl_propertylist *properties,
                                cxcptr object, cxcptr data,
                                GiFrameCreator creator);

cxint
giraffe_frame_save(const cpl_frame* frame,
                   const cpl_propertylist* properties,
                   cxcptr object, cxcptr data,
                   GiFrameCreator creator);


/*
 * Special frame creation utilities
 */

cpl_frame* giraffe_frame_create_image(GiImage* image, const cxchar* tag,
                                      cpl_frame_level level, cxbool save,
                                      cxbool update);
cpl_frame* giraffe_frame_create_table(GiTable* table, const cxchar* tag,
                                      cpl_frame_level level, cxbool save,
                                      cxbool update);
cxint giraffe_frame_attach_table(cpl_frame* frame, GiTable* table,
                                 const cxchar* tag, cxbool update);

/*
 * Frame retrieval utilities
 */

cpl_frame* giraffe_get_frame(const cpl_frameset* set, const cxchar* tag,
                             cpl_frame_group group);

cpl_frame* giraffe_get_slitgeometry(const cpl_frameset* set);


#ifdef __cplusplus
}
#endif

#endif /* GIFRAME_H */
