/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIFXCALIBRATION_H
#define GIFXCALIBRATION_H


#include <cpl_parameterlist.h>

#include <giimage.h>
#include <gitable.h>
#include <girebinning.h>
#include <giresponse.h>


#ifdef __cplusplus
extern "C" {
#endif

    struct GiFxCalibrationConfig {
        cxbool sky_subtraction;
        cxdouble max_dist;
    };

    typedef struct GiFxCalibrationConfig GiFxCalibrationConfig;


    GiFxCalibrationConfig*
    giraffe_fxcalibration_config_create(cpl_parameterlist* parameters);
    void giraffe_fxcalibration_config_destroy(GiFxCalibrationConfig* self);

    void giraffe_fxcalibration_config_add(cpl_parameterlist* parameters);


    GiTable* giraffe_select_flux_standard(const GiTable* catalog,
                                          const GiImage* spectra,
                                          cxdouble max_dist);

    cxint giraffe_calibrate_flux(GiResponse* result,
                                 const GiRebinning* spectra,
                                 const GiTable* fibers,
                                 const GiImage* flat,
                                 const GiTable* flux,
                                 const GiTable* extinction,
                                 const GiFxCalibrationConfig* config);


#ifdef __cplusplus
}
#endif

#endif /* GIFXCALIBRATION_H */
