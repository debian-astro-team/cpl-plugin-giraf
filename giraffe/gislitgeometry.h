/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GISLITGEOMETRY_H
#define GISLITGEOMETRY_H

#include <cxmacros.h>
#include <cxtypes.h>
#include <cxstring.h>

#include <gitable.h>


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief
 *   The slit geometry object definition.
 *
 * Structure stores a pointer to an array of cpl_matrix
 * and the (maximum) number of cpl_matrix that can be/are stored in it.
 *
 */

struct GiSlitGeometry {

    cpl_matrix *fps;
    cpl_matrix *rindex;

    cpl_matrix *xf;
    cpl_matrix *yf;

    /**
     * The (maximum) number of subslits
     */
    cxint nsubslits;

    /**
     * List of matrices containing fibre numbers, one matrix for each
     * subslit, matrices should be 1 column wide and the fibre numbers
     * should fill the matrix in ROW order
     */

    cpl_matrix **subslits;

};

typedef struct GiSlitGeometry GiSlitGeometry;


GiSlitGeometry *giraffe_slitgeometry_new(void);
GiSlitGeometry *giraffe_slitgeometry_create(GiTable *, cxbool);
GiSlitGeometry *giraffe_slitgeometry_duplicate(GiSlitGeometry *);
void giraffe_slitgeometry_delete(GiSlitGeometry *);

cxint giraffe_slitgeometry_size(GiSlitGeometry *);
void giraffe_slitgeometry_resize(GiSlitGeometry *, cxint);

void giraffe_slitgeometry_set(GiSlitGeometry *, cxint, cpl_matrix *);
cpl_matrix* giraffe_slitgeometry_get(GiSlitGeometry *, cxint);
void giraffe_slitgeometry_print(GiSlitGeometry *);


GiTable *giraffe_slitgeometry_load(const GiTable *fibers,
                                   const cxchar *filename, cxint pos,
                                   const cxchar *tag);
cpl_frame *giraffe_slitgeometry_save(const GiTable *slitgeometry);


#ifdef __cplusplus
}
#endif

#endif /* GISLITGEOMETRY_H */
