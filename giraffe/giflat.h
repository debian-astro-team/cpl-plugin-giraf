/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIFLAT_H
#define GIFLAT_H

#include <cxtypes.h>

#include <cpl_macros.h>

#include <giimage.h>
#include <giextraction.h>


#ifdef __cplusplus
extern "C" {
#endif


struct GiFlatConfig {
    cxbool load;
    cxbool apply;
    cxbool transmission;
};

typedef struct GiFlatConfig GiFlatConfig;


cxint giraffe_flat_apply(GiExtraction *spectra, const GiTable *fibers,
                         const GiImage *flat, const GiImage* errors,
                         GiFlatConfig *config);


/*
 * Convenience functions
 */

GiFlatConfig *giraffe_flat_config_create(cpl_parameterlist *list);
void giraffe_flat_config_destroy(GiFlatConfig *config);

void giraffe_flat_config_add(cpl_parameterlist *list);


#ifdef __cplusplus
}
#endif

#endif /* GIFLAT_H */
