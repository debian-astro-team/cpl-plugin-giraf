/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxmemory.h>

#include "giresponse.h"


/**
 * @defgroup giresponse Instrument Response
 *
 * TBD
 */

GiResponse*
giraffe_response_new(void)
{

    GiResponse* self = cx_calloc(1, sizeof *self);

    if (self != NULL) {
        self->response = NULL;
        self->efficiency = NULL;
    }

    return self;

}


void
giraffe_response_delete(GiResponse* self)
{

    if (self != NULL) {
        if (self->response != NULL) {
            giraffe_image_delete(self->response);
            self->response = NULL;
        }

        if (self->efficiency != NULL) {
            giraffe_table_delete(self->efficiency);
            self->efficiency = NULL;
        }

        cx_free(self);

    }

    return;

}

/**@{*/



/**@}*/
