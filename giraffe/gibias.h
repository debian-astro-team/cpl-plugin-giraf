/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIBIAS_H
#define GIBIAS_H

#include <cxstring.h>

#include <cpl_macros.h>
#include <cpl_parameterlist.h>
#include <cpl_matrix.h>

#include <giimage.h>


#ifdef __cplusplus
extern "C" {
#endif


enum GiBiasMethod {
    GIBIAS_METHOD_UNDEFINED,
    GIBIAS_METHOD_UNIFORM,
    GIBIAS_METHOD_PLANE,
    GIBIAS_METHOD_CURVE,
    GIBIAS_METHOD_PROFILE,
    GIBIAS_METHOD_MASTER,
    GIBIAS_METHOD_ZMASTER
};

typedef enum GiBiasMethod GiBiasMethod;


enum GiBiasOption {
    GIBIAS_OPTION_UNDEFINED,
    GIBIAS_OPTION_PLANE,
    GIBIAS_OPTION_CURVE
};

typedef enum GiBiasOption GiBiasOption;


enum GiBiasModel {
    GIBIAS_MODEL_UNDEFINED,
    GIBIAS_MODEL_FITTED,
    GIBIAS_MODEL_MEAN
};

typedef enum GiBiasModel GiBiasModel;


struct GiBiasConfig {
    GiBiasMethod method;
    GiBiasModel  model;
    GiBiasOption option;
    cxdouble     mbias;
    cxint        remove;
    cxchar*      areas;
    cxdouble     xdeg;
    cxdouble     ydeg;
    cxdouble     xstep;
    cxdouble     ystep;
    cxdouble     sigma;
    cxint        iterations;
    cxdouble     fraction;
};

typedef struct GiBiasConfig GiBiasConfig;


/*
 * Bias removal
 */

cxint
giraffe_bias_remove(GiImage* result_img, const GiImage* raw_frame,
                    const GiImage* master_bias, const GiImage* bad_pixels,
                    const cpl_matrix* biaslimits, const GiBiasConfig* config);


/*
 * Utility functions
 */

cpl_matrix* giraffe_get_raw_areas(const GiImage* image);
cxint giraffe_trim_raw_areas(GiImage* image);


/*
 * Convenience functions
 */

GiBiasConfig* giraffe_bias_config_create(cpl_parameterlist* list);
void giraffe_bias_config_destroy(GiBiasConfig* config);

void giraffe_bias_config_add(cpl_parameterlist* list);


#ifdef __cplusplus
}
#endif

#endif /* GIBIAS_H */
