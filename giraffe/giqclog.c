/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <cxstring.h>

#include "giqclog.h"


/**
 * @defgroup giqclog Quality Control Logs
 *
 * TBD
 */

/**@{*/

GiPaf *
giraffe_qclog_open(cxsize id)
{

    cx_string *name = cx_string_new();

    cpl_propertylist *properties = NULL;

    GiPaf *self = NULL;


    cx_string_sprintf(name, "qc%04" CX_PRINTF_FORMAT_SIZE_TYPE ".paf", id);

    self = giraffe_paf_new(cx_string_get(name), "QC1 parameters", NULL, NULL);

    cx_string_delete(name);
    name = NULL;

    properties = giraffe_paf_get_properties(self);
    cpl_propertylist_append_string(properties, "QC.DID", QC_DID);
    cpl_propertylist_set_comment(properties,"QC.DID", "QC1 dictionary");

    return self;

}


cxint
giraffe_qclog_close(GiPaf *qclog)
{

    if (qclog != NULL) {

        cxint status = 0;

        status = giraffe_paf_write(qclog);
        giraffe_paf_delete(qclog);

        if (status != 0) {
            return 1;
        }

    }

    return 0;

}
/**@}*/
