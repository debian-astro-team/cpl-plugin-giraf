/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIDARK_H
#define GIDARK_H

#include <giimage.h>


#ifdef __cplusplus
extern "C" {
#endif

    enum GiDarkMethod {
        GIDARK_METHOD_UNIFORM = 0,
        GIDARK_METHOD_MASTER  = 1,
        GIDARK_METHOD_ZMASTER = 2
    };

    typedef enum GiDarkMethod GiDarkMethod;


    struct GiDarkResults {
        cxdouble value;
        cxdouble expected;
        cxdouble mode;
        cxdouble maximum;
    };

    typedef struct GiDarkResults GiDarkResults;

    struct GiDarkConfig {
        GiDarkMethod method;
        cxdouble threshold;
    };

    typedef struct GiDarkConfig GiDarkConfig;


    cxint giraffe_subtract_dark(GiImage* image, const GiImage* dark,
                                const GiImage* bpixel, GiDarkResults* data,
                                const GiDarkConfig* config);

#ifdef __cplusplus
}
#endif

#endif /* GIDARK_H */
