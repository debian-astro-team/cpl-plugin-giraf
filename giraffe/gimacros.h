/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GIMACROS_H
#define GIMACROS_H

#include <cpl_macros.h>


#ifdef __cplusplus
extern "C" {
#endif


/*
 * Instrument hardware dependent quantities
 */

#define GISPECTRUM_MWIDTH_MEDUSA (9.3)
#define GISPECTRUM_MWIDTH_IFU    (5.8)


/*
 * Size of the Argus and IFU micro lens on sky (arcsec)
 */

#define GI_IFU_PIXSCALE (0.52)

#define GI_ARGUS_PIXSCALE_LOW  (GI_IFU_PIXSCALE)
#define GI_ARGUS_PIXSCALE_HIGH (0.30)


/*
 * Argus and IFU position angle offsets
 *
 * Argus offset is +90 degrees with respect to the position angle of the
 * adapter, and the position angle offset of the IFUs is 315 degrees with
 * respect to the negative orientation as given in the OzPoz table.
 */

#define GI_ARGUS_POSANG_OFFSET (90.)
#define GI_IFU_POSANG_OFFSET   (315.)


#define GIR_M_PIX_SET 0x0ffff

#define GI_MM_TO_NM 1000000.0
#define GI_NM_TO_MM 1./(GI_MM_TO_NM)


#ifdef __cplusplus
}
#endif

#endif /* GIMACROS_H */
