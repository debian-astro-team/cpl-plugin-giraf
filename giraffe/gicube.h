/*
 * This file is part of the GIRAFFE Pipeline
 * Copyright (C) 2002-2019 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GICUBE_H
#define GICUBE_H

#include <cxtypes.h>

#include <cpl_matrix.h>
#include <cpl_image.h>
#include <cpl_propertylist.h>


#ifdef __cplusplus
extern "C" {
#endif


    typedef struct GiCube GiCube;


    GiCube* giraffe_cube_new(void);
    GiCube* giraffe_cube_create(cxsize width, cxsize height, cxsize depth,
                                cxdouble* data);
    void giraffe_cube_delete(GiCube* self);

    cxsize giraffe_cube_get_width(const GiCube* self);
    cxsize giraffe_cube_get_height(const GiCube* self);
    cxsize giraffe_cube_get_depth(const GiCube* self);

    cxsize giraffe_cube_get_size(const GiCube* self);
    cxint giraffe_cube_set_size(GiCube* self, cxsize width, cxsize height,
                                cxsize depth);

    cxdouble* giraffe_cube_get_data(const GiCube* self);

    cxbool giraffe_cube_has_xaxis(const GiCube* self);
    cxbool giraffe_cube_has_yaxis(const GiCube* self);
    cxbool giraffe_cube_has_zaxis(const GiCube* self);
    cxbool giraffe_cube_has_wcs(const GiCube* self);

    cxint giraffe_cube_get_xaxis(const GiCube* self, cxdouble* start,
                                 cxdouble* step);
    cxint giraffe_cube_get_yaxis(const GiCube* self, cxdouble* start,
                                 cxdouble* step);
    cxint giraffe_cube_get_zaxis(const GiCube* self, cxdouble* start,
                                 cxdouble* step);

    cxint giraffe_cube_set_xaxis(GiCube* self, cxdouble start, cxdouble step);
    cxint giraffe_cube_set_yaxis(GiCube* self, cxdouble start, cxdouble step);
    cxint giraffe_cube_set_zaxis(GiCube* self, cxdouble start, cxdouble step);

    void giraffe_cube_clear_wcs(GiCube* self);

    cxint giraffe_cube_set_wcs(GiCube* self, const cpl_propertylist* axes,
                               const cpl_matrix* transformation);

    cxint giraffe_cube_sqrt(GiCube* self);

    cpl_image* giraffe_cube_integrate(const GiCube* self, cxdouble start,
                                      cxdouble end);

    cxint giraffe_cube_save(const GiCube* self, cpl_propertylist* properties,
                            const cxchar* filename, cxcptr data);


#ifdef __cplusplus
}
#endif

#endif /* GICUBE_H */
