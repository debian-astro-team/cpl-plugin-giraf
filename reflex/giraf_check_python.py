# ========================================================================================================
#
# John Pritchard
#
# ========================================================================================================
import reflex 
import sys 
import os
import json
from optparse import OptionParser
message=''
import_ok=1
try:
    import astropy
    from astropy.io import fits
    from astropy    import wcs
except ImportError:
    import_ok = 0
    message=message+'  astropy not found\n'
#try:
#    from astroquery.utils import TableList
#except ImportError:
#    import_ok = 0
#    message=message+'  astroquery not found\n'
try: 
    import numpy as np
    from numpy import array 
except ImportError:
    import_ok = 0
    message=message+'  numpy not found\n'
try:
    import scipy
except ImportError:
    import_ok = 0
    message=message+'  scipy not found\n'
    
    

if __name__ == '__main__':

#   MAIN MODULE ---

    
  parser = reflex.ReflexIOParser()
  parser.add_option("-i", "--trigger", dest="trigger")
  parser.add_output("-p", "--messages", dest="messages")
  #this line is needed but never used
  #parser.add_input("-v", "--fits_viewer", dest="fits_viewer", type='string', default="fv")
 # print 'this is it',sys.argv
  inputs  = parser.get_inputs()
  outputs = parser.get_outputs()
 
  
  if import_ok == 0: # in the case import fails
      outputs.messages = ('Warning: Failed to import some required Python modules:\n')+message+('\nCheck software requirements '
            ' https://www.eso.org/sci/software/pipelines/reflex_workflows/#software_prerequisites\n'
            ' in the GIRAF Reflex tutorial.\n'
            ' Terminating workflow execution.\n ')
      
  if import_ok == 1: # in the case import is fine
      outputs.messages = ('ok')
 
  parser.write_outputs()
  sys.exit()
  
