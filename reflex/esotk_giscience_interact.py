# ========================================================================================================
#
# John Pritchard
#
# Developed from esotk_fringe_obj_mask.py by Mark Neeser
#
# A python interactive window for use in the Austrian in-kind
# fringe-correction Reflex workflow.  This interactive window
# shows the results of the fringe-correction pipeline, allowing
# the User to compare the input image with the fringe-corrected
# image, and the fringe map.   From this interactive Actor the
# User can modify pipeline parameters and re-initiate the
# processing.
#
#
# ========================================================================================================


try:
    import os
    import re
    import sys
    import matplotlib
    matplotlib.use('wxagg')
    sys.path.append(os.path.dirname(__file__))
    from esotk_giss_DPM import giss_DataPlotterManager
    from esotk_giraf_sci_gui import runInteractiveApp
    import reflex
    import_sucess = True

    class DataPlotterManager(giss_DataPlotterManager):

        # This function specifies which are the parameters that should be presented
        # in the window to be edited.
        # Note that the parameter has to be also in the in_sop port (otherwise it
        # won't appear in the window)
        # The descriptions are used to show a tooltip. They should match one to one
        # with the parameter list
        # Note also that parameters have to be prefixed by the 'recipe name:'
        def setInteractiveParameters(self):
            paramList = list()
            paramList.append(reflex.RecipeParameter('giscience','bsremove-method',group='Bkg Sub',description='Backgruond subtraction method'))
            paramList.append(reflex.RecipeParameter('giscience','bsremove-yorder',group='Bkg Sub',description='Background subtraction yorder of 2-D polynomial fit'))
            paramList.append(reflex.RecipeParameter('giscience','extr-method',group='Extr',description="Extraction method: 'SUM', 'HORNE' or 'OPTIMAL'. <SUM | OPTIMAL | HORNE> [SUM]"))
            paramList.append(reflex.RecipeParameter('giscience','flat-apply',group='Flat',description="Apply or not the flat field correction. [TRUE]"))

            paramList.append(reflex.RecipeParameter('giscience','sky-applyTo',group='Sky',description="Apply Sky Subtraction to <None|All>. [All]"))
            paramList.append(reflex.RecipeParameter('giscience','sky-method',group='Sky',description="Sky Subtraction method <None|All|Best SNR|Nearest>. [All]"))
            paramList.append(reflex.RecipeParameter('giscience','sky-statistic',group='Sky',description="Sky Subtraction statisitic <median|mean>. [median]"))
            paramList.append(reflex.RecipeParameter('giscience','sky-numFibs',group='Sky',description="Sky Subtraction number of fibres to use <0|1-<numFibs>>. [0]"))
            paramList.append(reflex.RecipeParameter('giscience','sky-reject',group='Sky',description="Sky Subtraction Reject highest SNR fibs <true|false>. [true]"))
            paramList.append(reflex.RecipeParameter('giscience','sky-reject-thresh',group='Sky',description="Sky Subtraction rejection thresholds <8,12,15>. [8,12]"))
            paramList.append(reflex.RecipeParameter('giscience','sky-obj-ignore-list',group='Sky',description="List of sky 'objects' to ignore e.g. <sky1,sky2>. []"))
            return paramList

except ImportError:
    import_sucess = False
    print("Error importing reflex.py")

if __name__ == '__main__':
    if import_sucess :
        runInteractiveApp(DataPlotterManager)
