# ========================================================================================================
#
# John Pritchard
#
## This script allows a user to run the workflow without PyCosmic installed,
## then install PyCosmic and re-run the workflow and then get the PyCosmic
## results without having to remove the previous execution
#
# ========================================================================================================
# import the needed modules
import reflex 
import sys
import os
try :
    from astropy.io import fits as fits
    import_astropy_io=True
except:
    import pyfits as fits
    import_astropy_io=False
try:
    import PyCosmic
    import_sucess = True

except ImportError:
    import_sucess = False
    print("Error importing one or more of the module PyCosmic")

if __name__ == '__main__':
  
    # import reflex modules
    import reflex_interactive_app
    import sys

    #send output to EsoReflex:
    parser = reflex.ReflexIOParser()

    #Define ports:
    parser.add_option("-i", "--in_sof", dest="in_sof")
    parser.add_option("-e", "--enable", dest="enable", default='false')
    parser.add_output("-o", "--out_sof", dest="out_sof")

    inputs  = parser.get_inputs()
    outputs = parser.get_outputs()

    # Check if import failed or not
    if import_sucess and inputs.enable == "true" :
        # write out the in_sof as out_sof
        outputs.out_sof = inputs.in_sof
    else :
        # write out an empty sof
        outputs.out_sof = reflex.SetOfFiles(inputs.in_sof.datasetName,list())

    #send output to EsoReflex:
    parser.write_outputs()

    sys.exit()
