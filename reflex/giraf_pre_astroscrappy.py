# ========================================================================================================
#
# John Pritchard
#
## This script allows a user to run the workflow without AstroScrappy installed,
## then install AstroScrappy and re-run the workflow and then get the AstroScrappy
## results without having to remove the previous execution
#
# ========================================================================================================

# import the required modules
import reflex 
import sys
import os
try :
    from astropy.io import fits as pyfits
except:
    import pyfits
import logging
try:
    import astroscrappy
    import_sucess = True

except ImportError:
    import_sucess = False
    print("Error importing one or more of the astroscrappy")

if __name__ == '__main__':
  
    # import reflex modules
    import reflex_interactive_app
    import sys

    parser = reflex.ReflexIOParser()

    #Define ports:
    parser.add_option("-i", "--in_sof", dest="in_sof")
    parser.add_option("-e", "--enable", dest="enable", default='false')
    parser.add_output("-o", "--out_sof", dest="out_sof")

    inputs  = parser.get_inputs()
    outputs = parser.get_outputs()

    #Check if import failed or not
    if import_sucess and inputs.enable == "true" :
        # write out the in_sof as out_sof
        outputs.out_sof = inputs.in_sof
    else :
        # write out an empty sof
        outputs.out_sof = reflex.SetOfFiles(inputs.in_sof.datasetName,list())

    parser.write_outputs()

    sys.exit()
