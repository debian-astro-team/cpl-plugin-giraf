#!/usr/bin/env python3

import cpl
import os
import sys
import re

man_template = '''.TH {NAME} "{section}" "{version}" "{name}" "{pipeline} recipes"
.SH NAME
{name} - {synopsis}

.SH SYNOPSIS
esorex
.B {name}
[OPTIONS] FILE.sof

.SH DESCRIPTION
{description}

.SH OPTIONS
{options}
.PP
Note that it is possible to create a configuration file containing these
options, along with suitable default values. Please refer to the details
provided by the 'esorex --help' command.

.SH SEE ALSO
The full documentation for the {pipeline} pipeline can be downloaded as 
a PDF file using the following URL:
.IP
.B ftp://ftp.eso.org/pub/dfs/pipelines/giraffe/{pipeline}-pipeline-manual-2.11.pdf
.PP
An overview over the existing ESO pipelines can be found on the web page
\\fBhttps://www.eso.org/sci/software/pipelines/\\fR.
.PP
Basic documentation about the EsoRex program can be found at the esorex (1) 
man page.
.PP
It is possible to call the pipelines from python using the python-cpl package.
See \\fBhttps://packages.python.org/python-cpl/index.html\\fR for further
information.
.PP
The other recipes of the {pipeline} pipeline are
{seealso}

.SH VERSION
{name} {version}

.SH AUTHOR
{author} <{email}>

.SH BUG REPORTS
Please report any problems to {email}. Alternatively, you may send a report to the ESO User Support Department <usd-help@eso.org>.

.SH LICENSE
{license}
'''

par_template = '''.TP
\\fB--{name}\\fR \\fI<{type}>\\fR
{description}. The full name of this option for the EsoRex configuration
file is \\fB{fullname}\\fR [default = \\fI{default}\\fR].
'''

seealso_template = ".IR {name} ({section})"

fname_template ="{name}.{section}"

section = 7
pipeline = sys.argv[1]

cpl.Recipe.path = "recipes"
recipes = cpl.Recipe.list()
recipes.sort()

def param(recipe, template):
    return "".join(template.format(name = p.name,
                                   fullname = p.fullname,
                                   type = p.type.__name__,
                                   description = p.__doc__.replace("'", "\\'"),
                                   default = p.default)
                   for p in recipe.param)

def seealso(recipe, template):
    return ",\n".join(template.format(name = name,
                                      section=section)
                      for name,v in recipes 
                      if (name != recipe.__name__))
    
def manpage(recipe):
    description = recipe.description[1] 
    description = re.sub("-"*40+"+", "", description) \
                    .replace(".\n", ".\n.PP\n").replace("'", "\\'")
    description = re.sub("\n([A-Z][A-Z 0-9]+):?\n----+", "\n.SS \\1", description)
    return man_template.format(name = recipe.__name__,
                               NAME = recipe.__name__.upper(),
                               section = section,
                               version = recipe.__version__,
                               synopsis = recipe.description[0],
                               description = description,
                               seealso = seealso(recipe, seealso_template),
                               email = recipe.__email__,
                               author = recipe.__author__,
                               license = recipe.__copyright__,
                               pipeline = pipeline,
                               options = param(recipe, par_template))

for name, version in recipes:
    recipe = cpl.Recipe(name)
    f = open(os.path.join("man",
                          fname_template.format(name = name,
                                                section=section)), "w")
    f.write(manpage(recipe).replace('-', '\\-'))
    f.close()
